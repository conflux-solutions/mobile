import React, { Component } from 'react';
import {
  Text,
  View,
  Image,
  TouchableOpacity,
  Dimensions,
  ActivityIndicator,
  TouchableWithoutFeedback,
  Keyboard,
  SafeAreaView,
  TextInput
} from 'react-native';

import images from '@assets/imageGeneral'
import apiRoute from '@routes/apiRoute';
import AsyncStorage from '@react-native-community/async-storage';

const { width, height } = Dimensions.get('window');

export default class OwnerLogin extends Component {

  constructor(props) {
    super(props);
    this.state = {
        email: "",
        storeName: "",
        address: "",
        password: "",
        hidePass: true,
        loading: false
    };
  }

componentDidMount() {
}

async componentWillMount() {
    const KSI = await AsyncStorage.getItem('KSI');
    console.log("KSI", KSI)
    if(KSI == "true") {
        this.props.navigation.navigate('Categories')
    }
}

storeData = async (key, value) => {
  try {
    await AsyncStorage.setItem(key, value)
  } catch (e) {
    // saving error
    console.warn("cath error", e)
  }
}

goBack() {
    const { navigation } = this.props;
    navigation.goBack();
}

async verifyData(){

    const {email,password} = this.state;
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/ ;
    const userEmail = email
    
    if(userEmail == undefined || userEmail == ""){
        alert("Email requerido")
    } else if(password == undefined || password == ""){
        alert("Password requerido")
    }  else if(userEmail != undefined && userEmail != "" && reg.test(userEmail) === false) {
        alert('Email syntax invalid, it must to be like this: myemail@email.com');
    } else{
        const emailLower = userEmail.toLowerCase();
        this.setState({email: email})
        this.loginUser(emailLower, password)
    }
}

    loginUser(email, password){
        console.warn("email, password", email, password)
        this.setState({loading: true})
        var finalUrl = apiRoute.api + "users/signin"
        try{
            fetch(finalUrl,{
                method: 'POST',
                headers:{
                Accept: 'application/json',
                'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                email: email,
                password: password,
                }),
                }).then((response) => {
                    console.log("response", response)
                if(response.status == 200 || response.status == 201){
                    this.setState({loading: false})
                    response.json().then((json) => {
                    console.log("LOG", json)
                    if(!json.success) {
                        alert(json.error)
                    } else {
                        //alert("Success Login " + email)
                        console.log("json", json.token)
                        this.storeData('token', json.token)
                        this.storeData('userPass', password)
                        this.storeData('KSI', "true")
                        this.props.navigation.navigate('Geolocation')
                    }
                    })
                } else {
                  this.setState({loading: false})
                  alert("Hubo un problema")
                }
                })
            }catch(error){
                console.warn(error)
            }
        } 

  render() {
    const { state, goBack } = this.props.navigation;
    const emailValue =  this.props.navigation.getParam('emailValue', '')
    const passwordValue =  this.props.navigation.getParam('passwordValue', '')
    
    return (
      <View style={{backgroundColor:'white', flex:1}}>
        <SafeAreaView>
            <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
                <View>
                    <View style={{flexDirection:'column', alignItems:'center', marginTop:40, marginHorizontal:20}}>
                        <Image style={{width:100, height:150}} source={images.logoConflux} />  
                        <View style={{marginTop:50, width:width*0.7}}>
                            <View style={{flexDirection:'row'}}>
                                <Image style={{width:20, height:20}} source={images.loginUserIcon} /> 
                                <TextInput 
                                    style={{width:width*0.62, marginLeft:10}}
                                    autoCorrect={false}
                                    onChangeText={email => this.setState({email})}
                                    placeholder="EMAIL"
                                    autoCapitalize = 'none'
                                    >
                                </TextInput>
                            </View>
                            <View style={{marginTop:8, height: 0.5, backgroundColor:'grey'}} />
                            <View style={{marginTop:30, flexDirection:'row'}}>
                                <Image style={{width:20, height:20}} source={images.loginLockIcon} /> 
                                <TextInput 
                                    style={{width:width*0.62, paddingLeft:10}}
                                    autoCapitalize = 'none'
                                    autoCorrect={false}
                                    secureTextEntry={true}
                                    onChangeText={password => this.setState({password})}
                                    placeholder="PASSWORD"
                                    >
                                </TextInput>
                            </View>
                            <View style={{marginTop:8, height: 0.7, backgroundColor:'grey'}} />
                        </View> 
                        <View style={{alignItems:'center', marginTop: 100}}>
                            <TouchableOpacity 
                            onPress={()=>{this.verifyData()}}
                            style={{marginTop:40, borderRadius:5, alignItems:'center', justifyContent:'center', height:40, width:width*0.8, backgroundColor:'#3796F0'}}>
                                {
                                this.state.loading ? 
                                <ActivityIndicator size="small" color="white" />
                                :
                                <Text style={{color:'white'}}>SIGN IN</Text>
                                }
                            </TouchableOpacity>
                            <TouchableOpacity 
                                style={{marginTop:10}}
                                onPress={()=>{this.props.navigation.navigate('OwnerRegister')}}
                                >
                                <Text style={{color:'grey'}}>Don't have an account</Text>
                            </TouchableOpacity>
                            <TouchableOpacity 
                                style={{marginTop:10}}
                                onPress={()=>{this.props.navigation.navigate('OwnerRegister')}}
                                >
                                <Text style={{color:'black'}}>Register</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </TouchableWithoutFeedback>
          </SafeAreaView>
      </View>
    );
  }
}
