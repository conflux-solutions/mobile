import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Dimensions,
  StatusBar,
  Picker,
  ActivityIndicator,
  ScrollView,
  TouchableWithoutFeedback,
  Keyboard,
  Input,
  Alert,
  Button,
  Modal,
  SafeAreaView,
  TextInput
} from 'react-native';

import images from '@assets/imageGeneral'
import RNPickerSelect from 'react-native-picker-select';
import apiRoute from '@routes/apiRoute';
import AsyncStorage from '@react-native-community/async-storage';

const { width, height } = Dimensions.get('window');
var token = ""
var establishments = []

const colors = {
  red:'#DF6E6E',
  green:'#b7eb8f',
  yellow:'#DF6E6E',
  grey:'#CACACA',
  blue: '#2291E8'
}

const places = [
    {
      label: 'Pharmacie',
      value: 'Pharmacie',
    },
    {
      label: 'Hospital',
      value: 'Hospital',
    },
    {
      label: 'Grocery',
      value: 'Grocery',
    },
    {
      label: 'Bakery',
      value: 'Bakery',
    },
    {
      label: 'Supermarket',
      value: 'Supermarket',
    },
]

export default class OwnerPlatform extends Component {

  constructor(props) {
    super(props);
    this.state = {
        email: "",
        storeName: "",
        address: "",
        password: "",
        hidePass: true,
        loading: true,
        modalConfirData: false,
        estSelectedName: '',
        estSelectedID: '',
    };
  }

componentDidMount() {
  this.getData()
  token =  this.props.navigation.getParam('token', 'nothing sent')
  console.log("token sent", token)
  this.getEstablishments(token)
}

getData = async () => {
  try {
    const tokenStorage = await AsyncStorage.getItem('token')
    console.warn("tokenStorage", tokenStorage)
  } catch(e) {
    //error
  }
}
goBack() {
    const { navigation } = this.props;
    navigation.goBack();
}

  getEstablishments(token){
  var finalUrl = apiRoute.api + "owners/"
  try{
      fetch(finalUrl,{
          method: 'GET',
          headers:{
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': token
          },
          }).then((response) => {
          if(response.status == 200 || response.status == 201){
              this.setState({loading: false})
              response.json().then((json) => {
              if(!json.success) {
                  alert(json.error)
              } else {
                  console.log(json.establishments)
                  console.log("xcv", json.establishments.length)
                  establishments = json.establishments
                  this.setState({establishments: establishments})
              }
              console.log("establishmentsx2", establishments)
              })
          } else {
            alert("Ha ocurrido un error")
            this.setState({loading: false})
          }
          })
      }catch(error){
          console.warn(error)
      }
  } 

  render() {
    const { state, goBack } = this.props.navigation;
    return (
      <View style={{backgroundColor:'white', flex:1}}>
        <TouchableOpacity style={{borderWidth:0.5, borderColor:'grey', paddingVertical:5, paddingHorizontal:10, borderRadius:8, bottom:20, right:20, position:'absolute'}}>
            <Text> +  Añadir Store</Text>
        </TouchableOpacity>
        <SafeAreaView>
            <TouchableOpacity 
                //onPress={() => this.goBack()}
                onPress={()=>{this.props.navigation.navigate('Categories')}}
                style={{marginLeft:20, marginTop:20}}>
                <Image style={{ width:25, height:25}} source={images.exitIcon} />      
            </TouchableOpacity>
                <View>
                    <View style={{flexDirection:'column', alignItems:'center', marginHorizontal:20, marginBottom: 220}}>
                        <Text style={{fontSize:22}}>Owner Platform</Text>
                        <Text style={{marginTop:10, textAlign:'center', color:'grey'}}>Owner platform information</Text>
                        {
                        !this.state.loading ?
                        <ScrollView style={{marginBottom:100, marginTop: 40}}>
                          {/*====== ITEMS ======*/}
                          {
                          establishments.map((item) => {
                                return (
                                    <TouchableOpacity
                                    onPress={() => {this.setState({
                                                      modalConfirData:true,
                                                      estSelectedName: item.name,
                                                      estSelectedID: item.id
                                                    })
                                                  }
                                                }
                                    style={{borderColor:'#CACACA', borderWidth:0.5, width: width*0.9}}
                                    >
                                        <View style={{flex:0.9, paddingVertical:10, marginLeft:10}}>
                                            <Text>{item.name}</Text>
                                        </View>        
                                    </TouchableOpacity>
                                );
                            })
                          }
                        </ScrollView>
                        :
                        <ActivityIndicator style={{marginTop:20}} size="large" color="grey" />
                        } 
                    </View>
                </View>
          </SafeAreaView>

          {/*============ MODAL CONFIRMACION DATOS PARA PAGO =============*/}

            <Modal 
                visible={this.state.modalConfirData} 
                animationType='fade' 
                transparent={true}  >
              
              <View style={{flex: 1,alignItems: 'center', justifyContent:'center', flexDirection: 'column',  backgroundColor: 'rgba(0, 0, 0, 0.8)',}}>
                 
                <View style={{flexDirection:'column'}}>
                  <View style={{alignItems:'center', width: 300, height: 330, backgroundColor:'white'}}>
                    <Image style={{marginTop:20, width:260, height:250}} source={images.pharmacie2} />
                    <Text style={{marginTop:10}}>
                      {this.state.estSelectedName}
                    </Text>
                  </View>
                  <View style={{justifyContent:'space-between', flexDirection:'row', marginTop:20}}>
                      <TouchableOpacity style={{justifyContent:'center', alignItems:'center', backgroundColor:colors.blue, borderRadius:50, width:60, height:60}}>
                        <Image style={{ width:30, height:30}} source={images.editIcon} />
                      </TouchableOpacity>
                      <TouchableOpacity style={{justifyContent:'center', alignItems:'center', backgroundColor:colors.red, borderRadius:50, width:60, height:60}}>
                        <Image style={{ width:30, height:30}} source={images.deleteIcon} />
                      </TouchableOpacity>
                      <TouchableOpacity 
                      onPress={()=>{this.setState({modalConfirData:false})}}
                      style={{justifyContent:'center', alignItems:'center', backgroundColor:colors.grey, borderRadius:50, width:60, height:60}}>
                        <Image style={{ width:30, height:30}} source={images.closeIcon} />
                      </TouchableOpacity>
                  </View>
                </View>
              </View>
              
            </Modal>

      </View>
    );
  }
}


const styles = StyleSheet.create({
    button: {
      position: 'absolute',
      top: 20,
      padding: 10,
    },
    caption: {
      fontSize: 20,
      fontWeight: 'bold',
      alignItems: 'center',
    },
    container: {
      flex: 1,
      backgroundColor: 'white',
    },
    welcome: {
      fontSize: 20,
      textAlign: 'center',
      margin: 10,
    },
    instructions: {
      textAlign: 'center',
      color: '#333333',
      marginBottom: 5,
    },
    pickerStyle: {
        color:'red'
    }
  });