const languagesGeneral = {
    //OWNER
    ownerLogin: "Owner Login",
    pleaseEnterYourAcc: "Por favor ingrese sus datos de ingreso",
    register: "Registrar",
    signIn: "Ingresar",
    ownerRegister: "Registro para Owner",
    pleaseFillForm: "Por favor complete los siguientes datos",
    storeName: "Nombre establecimiento",
    storeAddress: "Dirección",
    setLocation: "Definir ubicación",
    hasBeenRegistered: "Ha sido registrado",
    success: "Éxito",
    automaticLogin: "Login automático",
    manualLogin: "Login manual",
    //
    userName: "Nombre",
    userEmail: "Email",
    userRegister: "Registrar nuevo usuario",
    congestionInfo: "Tiempo por congestión",
    detallesPlace: "Detalles de la ubicación",
    viewLocation: "Ver ubicación",
    // ENGLISH
    en: {
        userRegister: "User register",
        continue: 'Continue',
        selectLanguage: 'Select Language'
    },
    es: {
        userRegister: "Registro para usuario",
        continue: 'Continuar',
        selectLanguage: 'Seleccionar Idioma'
    },
    fr: {
        userRegister: "Registro para usuario",
        continue: 'Continuer',
        selectLanguage: 'Choisir la langue'
    }
    
};


export default languagesGeneral;
