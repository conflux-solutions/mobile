import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Dimensions,
  StatusBar,
  Picker,
  ActivityIndicator,
  ScrollView,
  Button,
  SafeAreaView,
  TextInput,
  ImageBackground
} from 'react-native';
import SideMenu from 'react-native-side-menu';
import Menu from './menu';
import images from '@assets/imageGeneral';
import apiRoute from '@routes/apiRoute';
import SFilter from '../components/Filter/squareFilter'

import AsyncStorage from '@react-native-community/async-storage';
import DeviceInfo from 'react-native-device-info';

var SELECTED_LAN = ""
var LAT_USER = 0
var LONG_USER = 0
var EXISTS_TOKEN = ""
const { width, height } = Dimensions.get('window');

const colors = {
    red:'#DF6E6E',
    green:'#b7eb8f',
    yellow:'#DF6E6E',
    grey:'#DF6E6E'
}

export default class Categories extends Component {

  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);

    this.state = {
      isOpen: false,
      selectedItem: 'About',
      pruebaItem: 'Pharmacie',
      placesVector: [],
      loading: true,
      radius: "500m",
      category: [],
      valueSearch: ""
    };
  }

componentDidMount() {

    let hasNotch = DeviceInfo.hasNotch();
    HASNOTCH = hasNotch

    const lat_user =  this.props.navigation.getParam('lat_user', 'nothing sent')
    const long_user =  this.props.navigation.getParam('long_user', 'nothing sent')
    console.warn("LAT_USER " + lat_user);
    console.warn("LONG_USER " + long_user);
    
    if(lat_user == null || lat_user == "nothing sent"){
      console.warn("Location already into storage")
    } else {
      this.storeData('lat_user', lat_user.toString())
      this.storeData('long_user', long_user.toString())
    }
    
    this.getCategories()
    //this.getPlaces("Pharmacie", 2.44, -76.61, this.state.radius)
    console.warn("this.state.placesVector", this.state.placesVector.length)
    console.warn("tokenStorage", this.state.tokenStorage)
}

storeData = async (key, value) => {
  try {
    await AsyncStorage.setItem(key, value)
  } catch (e) {
    // saving error
    console.warn("cath error", e)
  }
}


/// Method to get All categories
///
/// GET
///
/// No params
getCategories() {
    this.setState({loading: true})
    var finalUrl = apiRoute.api + "confluences/params" 
    try{
        fetch(finalUrl,{
            method: 'GET',
            }).then((response) => {
              console.log("responsex1", response)
            if(response.status == 200 || response.status == 201){
                this.setState({loading: false})
                response.json().then((json) => {
                    console.log("jsonx4", json.params[0].value)
                    console.log("lenght", json.params[0].value[0].en)
                    this.setState({category: json.params[0].value})
                })
            } else {
              alert("Ha ocurrido un error")
              this.setState({loading: false})
            }
            })
        }catch(error){
            console.warn(error)
        }
    } 


/// Method to get establishment by category and location
///
/// GET
///
/// Params: category, latitude, longitude, radius
///
/// Additional Information: 
/* Radius:
 -  500m = 500m
 -  1km = 1%20km
 -  2km = 2%20km
*/
///    
// getPlaces(category, lat, long, radius) {
//     console.warn("ENTRA", lat, long) 
//     this.setState({loading: true})
//     var finalUrl = apiRoute.api + "establishment?lat="+lat+"&lng="+long+"&radius="+radius+"&category="+category+"&offset=0&limit=10"
//     console.warn("finalUrl", finalUrl)

//     try{
//       fetch(finalUrl,{
//           method: 'GET',
//           }).then((response) => {
//           console.warn("response.status", response.status)
//           if(response.status == 200 || response.status == 201){
//               this.setState({loading: false})
//               response.json().then((json) => {
//                   json.values.forEach(element => {
//                       //console.warn("element", element)
//                   });
//                   this.setState({placesVector: json.values, loading: false})
//                   console.warn("placesVector", this.state.placesVector)
                  
//               })
//           } else {
//             this.setState({loading: false})
//             alert("Error obteniendo Categorias")
//           }
//           })
//       }catch(error){
//           console.warn(error)
//       }
//   } 


  onSelect = data => {
    
    // if(data.selected) {
    //   this.setState({
    //     showFinalPlace: true
    //   })
    // }
    this.setState(data);

    //this.getPlaces("Pharmacie", 48.8588377, 2.2770203, "500m")
  };

  toggle() {
      console.warn("1")
    this.setState({
      isOpen: !this.state.isOpen,
    });
  }

  updateMenuState(isOpen) {
    this.setState({ isOpen });
  }

  clearItem = async () => {
    this.storeData('token', "")
    this.storeData('KSI', "")
    this.props.navigation.navigate('Start')
  };

  // this.updateMenuState(!this.state.isOpen)
  onMenuItemSelected = item => {
    console.warn("item", item)
    if(item=="DETECT_LOCATION") {
      this.props.navigation.navigate('Geolocation')
    } else if(item=="OWNER_REGISTER") {
      this.props.navigation.navigate('OwnerRegister')
    } else  if(item=="SCAN_QR") {
      this.props.navigation.navigate('ReadQRS')
    } else if (item=="OWNER_LOGIN") {
      this.props.navigation.navigate('OwnerLogin')
    } else if (item=="LOG_OUT"){
      console.warn("LOG_OUT")
      this.clearItem()
    } else if (item=="MY_CHECKIN") {
      this.props.navigation.navigate('MyCheckIn')
    }
    this.setState({
      isOpen: false,
      selectedItem: item,
    });
  }

  searchFunction = value => {
    console.warn("value", value)
    this.setState({valueSearch: value})
  }

  async componentWillMount() {
    const id_person = await AsyncStorage.getItem('id_person');
    console.warn("id_person", id_person)
    const tokenStorage = await AsyncStorage.getItem('token');
    EXISTS_TOKEN = tokenStorage
    const lat_user = await AsyncStorage.getItem('lat_user');
    const long_user = await AsyncStorage.getItem('long_user');
    LAT_USER = parseFloat(lat_user)
    LONG_USER = parseFloat(long_user)
    console.warn("lat_user_storage", lat_user)
    console.warn("long_user_storage", long_user)    
    // selected_language
    const selected_language = await AsyncStorage.getItem('selected_language');
    console.log("selected_language", selected_language)
    SELECTED_LAN = selected_language
  }
  
  render() {

    const lat_user = LAT_USER
    const long_user = LONG_USER
    
    console.warn("EXISTS_TOKEN " + EXISTS_TOKEN);
  
    const menu = <Menu onItemSelected={this.onMenuItemSelected} /> 

    const placeholder = {
        label: 'Select a sport..',
        value: null,
        color: '#9EA0A4',
    };
    const placeholderDistance = {
        label: 'Select a distance..',
        value: null,
        color: '#9EA0A4',
    };

    return (
      <SafeAreaView>
          <StatusBar barStyle = "dark-content" hidden = {false} backgroundColor = "#00BCD4" translucent = {true}/>
          <Text style={{marginTop:height}}></Text>
        <SideMenu
          menu={menu}
          isOpen={this.state.isOpen}
          onChange={isOpen => this.updateMenuState(isOpen)}
        >
          <View style={styles.container}>
                  <View style={{marginTop:20, width: width, height: 100}}>
                      <View>
                          <View style={{flexDirection:'row'}}>
                              <View style={{marginHorizontal:10, width:width*0.1,  marginTop:20}}>
                                <Image style={{width:48, height:70}} source={images.logoIconText} />
                              </View>
                              <View style={{marginLeft:10, width:width*0.66,  marginTop:26}}>
                                <View style={{height:40, marginHorizontal:10, borderRadius:7, alignItems:'center',  flexDirection:'row'}}>
                                
                                </View>
                              </View>
                              <View style={{marginRight:20, width:width*0.1, marginTop:25}}>
                                  <TouchableOpacity
                                  onPress={()=>{this.updateMenuState(!this.state.isOpen)}}
                                  >
                                      <Image style={{width:44, height:42}} source={images.menuIcon} />
                                  </TouchableOpacity>
                              </View>
                          </View>
                      </View>
                  </View>
                
                
                {/*========================== END TAB BAR ====================================*/}
                <View style={{flex: 1}}>
                  <View style={{alignItems:'center'}}>
                    <Text style={{fontSize:20}}>Welcome to Conflux</Text>
                    <Text style={{marginTop:5, fontSize:17, color:'grey'}}>Search for the place you want to visit</Text>
                  </View>
                  <View style={{alignItems:'center', marginTop:50, flexDirection:'column'}}>
                    <View style={{flexDirection:'row', height:50, width:width*0.9, borderWidth:1.5, borderColor:'#2177BB', borderRadius:30}}>
                      <Image style={{marginTop:15, marginLeft:10, width:20, height:20}} source={images.searchIconBlue} />
                      <TextInput 
                          style={{marginTop:15, width:width*0.7, paddingHorizontal:10, height:20}} 
                          onChangeText={text => this.searchFunction(text)}
                          placeholder={"Search"}
                          >
                        </TextInput>
                    </View>
                    <TouchableOpacity
                    onPress={()=>{this.props.navigation.navigate('MyCheckIn')}}
                    >
                      <Text style={{color:'#2177BB', marginLeft:width*0.6, marginTop:10}}>Use my Location</Text>
                    </TouchableOpacity>
                  </View>
                {
                this.state.valueSearch.length > 0 ?
                <ScrollView style={{ flex: 1, marginTop:40}}>
                  <View style={{flexWrap: 'wrap', flexDirection:'row', marginHorizontal:10, marginBottom:100, marginTop:40}}>
                    <Text>Resultados para: {this.state.valueSearch}</Text>
                    <View>

                    </View>
                  </View>
                </ScrollView>
                :
                <ScrollView style={{marginLeft:5, flex: 1}}>
                    {
                    !this.state.loading ?
                    
                    <View style={{justifyContent:'center', flexWrap: 'wrap', flexDirection:'row', marginBottom:100, marginTop:10}}>
                        {
                            this.state.category.map((item) => {
                                return (
                                    <TouchableOpacity
                                        onPress={()=>{this.props.navigation.navigate('FilterPlaces', {
                                            id_category: item._id, 
                                            name_category: item, 
                                            lat_user: lat_user, 
                                            selected_language: SELECTED_LAN,
                                            long_user: long_user})}}>
                                        <SFilter title={item} lan={SELECTED_LAN} const={width} />
                                    </TouchableOpacity>
                                )})
                        }
                    </View>
                    
                    :
                    <ActivityIndicator style={{marginTop:20}} size="large" color="grey" />
                    }
                </ScrollView>
                }
                </View>
          </View>
        </SideMenu>
      </SafeAreaView>
    );
  }
}


const styles = StyleSheet.create({
    button: {
      position: 'absolute',
      top: 20,
      padding: 10,
    },
    caption: {
      fontSize: 20,
      fontWeight: 'bold',
      alignItems: 'center',
    },
    container: {
      flex: 1,
      backgroundColor: 'white',
    },
    welcome: {
      fontSize: 20,
      textAlign: 'center',
      margin: 10,
    },
    instructions: {
      textAlign: 'center',
      color: '#333333',
      marginBottom: 5,
    },
    pickerStyle: {
        color:'red'
    }
  });