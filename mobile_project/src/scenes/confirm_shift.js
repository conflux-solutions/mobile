
import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  Image, 
  Alert,
  ImageBackground,
  Animated,
  ActivityIndicator
} from 'react-native';

import images from '@assets/imageGeneral';
import MapView, { Marker, ProviderPropType } from 'react-native-maps';
import Geolocation from '@react-native-community/geolocation';
import apiRoute from '@routes/apiRoute';
import AsyncStorage from '@react-native-community/async-storage';

const { width, height } = Dimensions.get('window');

var LAT_USER = 0
var LONG_USER = 0
const ASPECT_RATIO = width / height;
var LATITUDE = 0;
var LONGITUDE = 0;
var CATEGORY = "";
const LATITUDE_DELTA = 0.05;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
var INFO = []
let id = 0;
let DATE = ""
let TIME = ""
let DATE_FORMAT = ""


class Confirm_shift extends React.Component {

    componentDidMount() {
      this.getToken()
    }

    async getToken() {
      const tokenStorage = await AsyncStorage.getItem('token');
      TOKEN_USER = tokenStorage
      console.warn("tokenStorage", tokenStorage)
    }

    goBack() {
        const { navigation } = this.props;
        navigation.goBack();
    }

    setNewShift() {
      // console.log("DATE_FORMAT", DATE_FORMAT)
      // console.log("ID_ESTABLISHMENT", INFO._id)
      // console.log("TOKEN_USER", TOKEN_USER)
      this.setState({loading:true})
      var finalUrl = apiRoute.api + "shifts/new"
      try{
          fetch(finalUrl,{
              method: 'POST',
              headers:{
              Accept: 'application/json',
              'Content-Type': 'application/json',
              'Authorization': TOKEN_USER
              },
              body: JSON.stringify({
              id_establishment: INFO._id,
              shift_date: DATE_FORMAT,
              comments: ""
              }),
              }).then((response) => {
              console.log("responsex3", response)
              if(response.status == 200 || response.status == 201){
                  response.json().then((json) => {
                    if(json.success) {
                      this.setState({loading:false, confirmShift:true})
                    } else {
                      alert("Something went wrong")
                    }
                  })
              } else {
                response.json().then((json) => {
                  console.log("JSON NO 200: ", json)
                  alert(json.message)
                  this.setState({loading:false})
                })
              }
              })
      }catch(error){
          console.warn(error)
      }
    }
    
    watchID: ?number = null;

    constructor(props) {
        super(props);
        this.animatedValue = new Animated.Value(0)
        /// DATA SENT
        //*
        //*
        /// USER LOCATION
        const lat_user =  this.props.navigation.getParam('lat_user', 'nothing sent')
        const long_user =  this.props.navigation.getParam('long_user', 'nothing sent')
        LAT_USER = lat_user
        LONG_USER = long_user
        console.warn("LAT_USER " + lat_user);
        console.warn("LONG_USER " + long_user);
        //*
        //*
        /// PLACE INFO
        const info =  this.props.navigation.getParam('info', 'nothing sent')
        INFO = info
        console.warn("LONG " + info.location.longitude);
        console.warn("LAT " + info.location.latitude);
        console.warn("LAT " + info.name);
        //*
        //*
        /// INFO DATE & TIME SHIFT
        const timeNDate =  this.props.navigation.getParam('timeNDate', 'nothing sent timeNDate')
        const timeFormated =  this.props.navigation.getParam('time', 'nothing sent timeFormated')
        var auxTimeNDate = timeNDate.split(" ")
        DATE = auxTimeNDate[0]
        TIME = auxTimeNDate[1]+auxTimeNDate[2]
        console.warn("auxTimeNDate", auxTimeNDate)
        DATE_FORMAT = timeFormated
        //*
        //*
        /// SET DATA PLACE
        CATEGORY = info.category
        LATITUDE = info.location.latitude
        LONGITUDE = info.location.longitude
        
        this.state = {
            hideInfo: true,
            loading: false,
            confirmShift: false,
            region: {
                latitude: LATITUDE,
                longitude: LONGITUDE,
                latitudeDelta: LATITUDE_DELTA,
                longitudeDelta: LONGITUDE_DELTA,
              },
            regionUser: {
                latitude: LAT_USER,
                longitude: LONG_USER,
                latitudeDelta: LATITUDE_DELTA,
                longitudeDelta: LONGITUDE_DELTA,
            },
          markers: [],
        };
      }

      render() {
        return (
          <View style={styles.container}>   
            {LATITUDE == 0 ?  
            <ActivityIndicator style={{marginBottom:height/2-30}} size="large" color="grey" />
            :
            <MapView
              provider={this.props.provider}
              style={styles.map}
              initialRegion={this.state.region}
              onPress={e => console.log(e)}>
                <Marker
                    key="1"
                    coordinate={this.state.region}
                    pinColor= "red">
                    <Image
                        source={CATEGORY=="Pharmacie"?images.pharmacyIcon:images.shopIcon}
                        style={{height: 75, width: 75}} />
                </Marker>
                <Marker
                    key="2"
                    coordinate={this.state.regionUser}
                    pinColor= "red">
                    <Image source={images.houseIcon} style={{height: 75, width: 75}} />
                </Marker>
            </MapView>
            }
            <TouchableOpacity 
            onPress={() => this.goBack()}
            style={{alignItems:'center', justifyContent: 'center', 
                                    backgroundColor:'white', width:40, height:40, 
                                    borderRadius:50, position:'absolute', 
                                    left:20, top:50,shadowColor: "#000",
                                    shadowOffset: {
                                        width: 0,
                                        height: 4,
                                    },
                                    shadowOpacity: 0.30,
                                    shadowRadius: 4.65,
                                    elevation: 8}}>
                <Image style={{marginLeft:-4, width:25, height:25}} source={images.backIcon} />
            </TouchableOpacity>
            <View style={{backgroundColor:'white', borderTopLeftRadius:20, borderTopRightRadius:20, flexDirection:'column', width:width*0.9, height: this.state.hideInfo ? height*0.45 : 55}}>
              <TouchableOpacity 
                  onPress={()=>{this.setState({hideInfo:!this.state.hideInfo})}}
                  style={{ flexDirection:'row', 
                  justifyContent:'space-between',
                  alignItems:'center',
                  shadowOffset: {
                      width: 0,
                      height: 4,
                  },
                  shadowOpacity: 0.30,
                  shadowRadius: 4.65,
                  elevation: 8, height:50, 
                  borderTopLeftRadius:20, borderTopRightRadius:20, 
                  backgroundColor:'white'}}>
                  <TouchableOpacity
                  onPress={() => this.goBack()}>
                      <Image style={{marginLeft:10, width:22, height:18}} source={images.backArrowIcon} />
                  </TouchableOpacity>
                  <Text>
                      Summary
                  </Text>
                  <TouchableOpacity
                  onPress={()=>{this.setState({hideInfo:!this.state.hideInfo})}}>
                      <Image style={{marginRight:20, width:25, height:25}} source={images.closeIconDark} />
                  </TouchableOpacity>
              </TouchableOpacity>
            <View style={{paddingVertical:30, flexDirection:'column', alignItems:'center', justifyContent: 'space-between', borderRightWidth:2, borderLeftWidth:2, borderColor:'#E5E2E2', marginTop:0.5,  height:height*0.39}}>
                    {
                    this.state.confirmShift ?
                    <View style={{marginTop:20, alignItems:'center'}}>
                      <Image style={{width:60, height:60}} source={images.checkConfirmIcon} />
                      <Text style={{color:'green'}}>Check-In Confirmed!</Text>
                    </View>
                    :
                    <View>
                        <Text style={{color:'grey'}}>
                            ESTABLISHMENT
                        </Text>
                        <Text style={{marginTop:5}}>
                            {INFO.name}
                        </Text>
                        <View style={{height:1, backgroundColor:'#B9B9B9', width:width*0.7}} />
                        <Text style={{marginTop:15, color:'grey'}}>
                            DATE
                        </Text>
                        <Text style={{marginTop:5}}>
                            {DATE}
                        </Text>
                        <View style={{height:1, backgroundColor:'#B9B9B9', width:width*0.7}} />
                        <Text style={{marginTop:15, color:'grey'}}>
                            TIME
                        </Text>
                        <Text style={{marginTop:5}}>
                            {TIME}
                        </Text>
                        <View style={{height:1, backgroundColor:'#B9B9B9', width:width*0.7}} />
                    </View>
                    }
                    { 
                    this.state.loading ? 
                    <ActivityIndicator size="large" color="grey" />
                    :
                    this.state.confirmShift ?
                    <View style={{width:width*0.6, marginTop:10, justifyContent:'center', flexDirection:'row'}}>
                        <TouchableOpacity 
                            onPress={()=>{this.props.navigation.navigate('MyCheckIn')}}
                            style={{borderRadius:25, paddingVertical:10, paddingHorizontal:20, borderWidth:2, borderColor:'#2177BB'}}>
                            <Text style={{fontSize:20, fontWeight:'bold', color:'#2177BB'}}>Confirm</Text>
                        </TouchableOpacity>
                    </View>
                    :
                    <View style={{width:width*0.6, marginTop:10, justifyContent:'space-between', flexDirection:'row'}}>
                        <TouchableOpacity 
                        onPress={() => this.goBack()}
                        style={{paddingVertical:10, paddingHorizontal:20}}>
                            <Text style={{fontSize:20}}>Cancel</Text>
                        </TouchableOpacity>
                        <TouchableOpacity 
                            onPress={()=>{this.setNewShift()}}
                            style={{borderRadius:25, paddingVertical:10, paddingHorizontal:20, borderWidth:2, borderColor:'#2177BB'}}>
                            <Text style={{fontSize:20, fontWeight:'bold', color:'#2177BB'}}>Confirm</Text>
                        </TouchableOpacity>
                    </View>
                    }
                </View>
            </View>
          </View>
        );
      }
    }


Confirm_shift.propTypes = {
    provider: ProviderPropType,
  };
  
  const styles = StyleSheet.create({
    container: {
      ...StyleSheet.absoluteFillObject,
      justifyContent: 'flex-end',
      alignItems: 'center',
    },
    map: {
      ...StyleSheet.absoluteFillObject,
    },
    bubble: {
      backgroundColor: 'rgba(255,255,255,0.7)',
      width: width*0.9,
      alignItems: 'center',
      justifyContent: 'center'
    },
    latlng: {
      width: 200,
      alignItems: 'stretch',
    },
    button: {
      width: 80,
      paddingHorizontal: 12,
      alignItems: 'center',
      marginHorizontal: 10,
    },
    buttonContainer: {
      flexDirection: 'row',
      backgroundColor: 'white',
      borderRadius: 20,
      height:110,
      width: width*0.9,
      alignItems: 'center',
      justifyContent: 'center',
      marginBottom:10,
      borderBottomWidth: 0,
      shadowColor: 'gray',
      shadowOffset: { width: 0, height: 3 },
      shadowOpacity: 0.8,
      shadowRadius: 2,
      elevation: 1,
    },
    buttonConfirm: {
      flexDirection: 'row',
      backgroundColor: '#F3C610',
      height:60,
      marginBottom: 80,
      width: width*0.9,
      borderRadius: 10,
      alignItems: 'center',
      justifyContent: 'center',
      shadowColor: 'gray',
      shadowOffset: { width: 0, height: 3 },
      shadowOpacity: 0.8,
      shadowRadius: 2,
      elevation: 1
    }
  });
  
  export default Confirm_shift;