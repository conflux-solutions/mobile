import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Dimensions,
  StatusBar,
  Picker,
  ActivityIndicator,
  ScrollView,
  Button,
  ImageBackground,
  SafeAreaView,
  TextInput
} from 'react-native';
import SideMenu from 'react-native-side-menu';
import Menu from './menu';
import RNPickerSelect from 'react-native-picker-select';
import images from '@assets/imageGeneral';
import apiRoute from '@routes/apiRoute';

var LAT_USER = 0
var LONG_USER = 0

var NAME_CAT = ""
const { width, height } = Dimensions.get('window');

const colors = {
    red:'#DF6E6E',
    green:'#b7eb8f',
    yellow:'#DF6E6E',
    grey:'#DF6E6E'
}
const distance = [
    {
        label: '500m',
        value: '500m',
    },
    {
      label: '1000m',
      value: '1 Km',
    },
    {
      label: '2000m',
      value: '2 Kms',
    },
    {
      label: '4000m',
      value: '4 Kms',
    },
    {
      label: '8000m',
      value: '8 Kms',
    },
  ];

const places = [
    {
      label: 'Farmacia',
      value: 'Pharmacie',
    },
    {
      label: 'Hospital',
      value: 'Hospital',
    },
    {
      label: 'Tienda/Viveres',
      value: 'Grocery',
    },
    {
      label: 'Panadería',
      value: 'Bakery',
    },
    {
      label: 'Super Mercado',
      value: 'Supermarket',
    },
]

export default class FilterPlaces extends Component {

  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);

    this.state = {
      isOpen: false,
      selectedItem: 'About',
      pruebaItem: 'Pharmacie',
      placesVector: [],
      loading: true,
      radius: "500m",
      firstTime: true,
      loading: true
    };
  }

componentDidMount() {
    this.getSentData()
    console.warn("this.state.placesVector", this.state.placesVector.length)
}

/// Function to get sent data from categories screen
///
/// Data sent lat_user, long_user, id_category, name_category
///
getSentData() {
    const lat_user =  this.props.navigation.getParam('lat_user', 'nothing sent')
    const long_user =  this.props.navigation.getParam('long_user', 'nothing sent')
    
    const name_category =  this.props.navigation.getParam('name_category', 'nothing sent')
    
    const selected_language =  this.props.navigation.getParam('selected_language', 'no language sent')
    
    console.log("LAT_USER_FILTER_PLACES" + lat_user);
    console.log("LONG_USER_FILTER_PLACES" + long_user);
    
    console.warn("NAME_CAT " + name_category);
    LAT_USER = lat_user
    LONG_USER = long_user
    
    if (selected_language == "en") {
      NAME_CAT = name_category.en
    } else if(selected_language == "es") {
      NAME_CAT = name_category.es
    } else {
      NAME_CAT = name_category.fr
    }
    
    
    this.setState({selectCategory: NAME_CAT, loading: false})
    this.getPlaces(NAME_CAT, LAT_USER, LONG_USER, this.state.radius)
}

/// Method to get establishment by category and location
///
/// GET
///
/// Params: category, latitude, longitude, radius
///
/// Additional Information: 
/* Radius:
 -  500m = 500m
 -  1km = 1%20km
 -  2km = 2%20km
*/
///    
getPlaces(category, lat, long, radius) {
    console.warn("Data", category, lat, long, radius)
    this.setState({loading: true})
    var finalUrl = apiRoute.api + "establishment?lat="+lat+"&lng="+long+"&radius="+radius+"&category="+category+"&offset=0&limit=10"
    console.warn("finalUrl", finalUrl)
    console.log("finalUrl", finalUrl)

    fetch(finalUrl, {
        method: 'GET'
        //Request Type 
    })
    .then((response) => response.json())
    //If response is in json then in success
    .then((responseJson) => {
        //Success 
        //console.warn("responseJson", responseJson.values)
        
        responseJson.values.forEach(element => {
            //console.warn("element", element)
            console.log("responseJsonx2", element.location)
        });
        this.setState({placesVector: responseJson.values, loading: false})
        console.warn("placesVector", this.state.placesVector)
        console.log("placesVector", this.state.placesVector)
    })
    //If response is not in json then in error
    .catch((error) => {
        //Error 
        //alert(JSON.stringify(error));
        console.error(error);
    });
  }

  onSelect = data => {
    this.setState(data);
    this.getPlaces("Pharmacie", LAT_USER, LONG_USER, "500m")
  };

  toggle() {
      console.warn("1")
    this.setState({
      isOpen: !this.state.isOpen,
    });
  }

  updateMenuState(isOpen) {
    this.setState({ isOpen });
  }

  goBack() {
    const { navigation } = this.props;
    navigation.goBack();
}

onMenuItemSelected = item => {
  console.warn("item", item)
  if(item=="DETECT_LOCATION") {
    this.props.navigation.navigate('Geolocation')
  } else if(item=="OWNER_REGISTER") {
    this.props.navigation.navigate('OwnerRegister')
  } else  if(item=="SCAN_QR") {
    this.props.navigation.navigate('ReadQRS')
  } else if (item=="OWNER_LOGIN") {
    this.props.navigation.navigate('OwnerLogin')
  } else if (item=="LOG_OUT"){
    console.warn("LOG_OUT")
    this.clearItem()
  } else if (item=="MY_CHECKIN") {
    this.props.navigation.navigate('MyCheckIn')
  }
  this.setState({
    isOpen: false,
    selectedItem: item,
  });
}

  searchFunction = value => {
    console.warn("value", value)
    this.setState({valueSearch: value})
  }

  roundHour(value) {
    var response = ""
    response = value < 9 ? '0'+value : value
    return response
  }


  render() {

    const menu = <Menu onItemSelected={this.onMenuItemSelected} />;
    const placeholder = {
        label: 'Select a sport..',
        value: null,
        color: '#9EA0A4',
    };
    const placeholderDistance = {
        label: 'Select a distance..',
        value: null,
        color: '#9EA0A4',
    };

    return (
      <SafeAreaView>
          <StatusBar barStyle = "dark-content" hidden = {false} backgroundColor = "#00BCD4" translucent = {true}/>
          <Text style={{marginTop:height}}>CAM</Text>
        <SideMenu
          menu={menu}
          isOpen={this.state.isOpen}
          onChange={isOpen => this.updateMenuState(isOpen)}
        >
          <View style={styles.container}>
                <ImageBackground style={{width:width, height:120}} source={images.gradientHeader}>
                  <View style={{marginTop: 30, width: width, height: 70, }}>
                      <View>
                          <View style={{flexDirection:'row'}}>
                              <TouchableOpacity onPress={() => (this.goBack())}>
                                <View style={{marginLeft:15, width:width*0.1,  marginTop:15}}>
                                  <Image style={{width:45, height:55}} source={images.logoIcon} />
                                </View>
                              </TouchableOpacity>
                              <View style={{marginLeft:10, width:width*0.68,  marginTop:20}}>
                                <View style={{height:45, marginHorizontal:10, borderRadius:7, alignItems:'center', backgroundColor:'#0E4877', flexDirection:'row'}}>
                                
                                  <TextInput 
                                    style={{color:'white', marginTop:4, width:width*0.7, paddingHorizontal:10, height:30}} 
                                    maxLength={20}
                                    onChangeText={text => this.searchFunction(text)}
                                    >
                                  </TextInput>
                                  <Image style={{position:'absolute', right:12, width:25, height:25}} source={images.searchIcon} />

                                </View>
                              </View>
                              <View style={{marginHorizontal:10, width:width*0.1, marginTop:22}}>
                                  <TouchableOpacity
                                  onPress={()=>{this.updateMenuState(!this.state.isOpen)}}
                                  >
                                      <Image style={{width:42, height:40}} source={images.menuIconWhite} />
                                  </TouchableOpacity>
                              </View>
                          </View>
                      </View>
                  </View>
                </ImageBackground>
                
                {
                  !this.state.loading?

                <View style={{flexDirection:'row', marginTop:20, justifyContent:'space-between'}}>
                    <View style={{marginLeft:10}}>
                        <View style={{ justifyContent: 'center',  
                                        borderWidth:0.25, 
                                        borderColor: 'grey',
                                        width:180, height:30}}>
                            
                                <View style={{marginLeft:10}}>
                                  <RNPickerSelect
                                      placeholder={{
                                        label: `${this.state.selectCategory}`,
                                        value: null,
                                    }}
                                    style={styles.pickerStyle}
                                    onValueChange={(value) => {this.setState({selectCategory: value}), this.getPlaces(value, LAT_USER, LONG_USER, this.state.radius)}}
                                    items={places}
                                    Icon={() => {
                                        return <Image style={{marginRight:5, width:25, height:20}} source={images.downIcon} /> ;
                                      }}
                                />
                                </View>
                            </View> 
                        
                    </View> 
                    <View style={{marginRight:10}}>
                        <View style={{ justifyContent: 'center',  
                                        borderWidth:0.25, 
                                        borderColor: 'grey',
                                        width:120, height:30}}>
                            <View style={{flexDirection:'row'}}>
                                <View style={{marginLeft:10, flex:0.99}}>
                                    <RNPickerSelect
                                        placeholder={{
                                          label: `${this.state.radius}`,
                                          value: null
                                        }}
                                        onValueChange={(value) => {this.setState({radius:value}), this.getPlaces(this.state.selectCategory, LAT_USER, LONG_USER, value)}}
                                        items={distance}
                                        Icon={() => {
                                            return <Image style={{marginRight:5, width:25, height:20}} source={images.downIcon} /> ;
                                          }}
                                    />
                                </View>
                            </View>
                        </View>
                    </View>
                </View>
                : null }
                {/*=== Float QR Button START =====*/}
                <TouchableOpacity 
                onPress={()=>{this.props.navigation.navigate('ReadQRS')}}
                style={{
                        position:'absolute', 
                        zIndex: 3, //ioS
                        elevate: 3,  //Android
                        right:20, bottom: height < 700 ? 50 : 100,
                        borderRadius:50, alignItems:'center', 
                        justifyContent: 'center', 
                        width:55, height:55, 
                        backgroundColor:'#2177BB'}}>
                  <Image style={{width:35, height:35}} source={images.qrBKBlue} />
                </TouchableOpacity>
                {/*=== Float QR Button END =====*/}
                <View style={{marginTop:10, marginBottom:10, marginLeft:10}}>
                    <Text><Text style={{fontWeight:'bold'}}>{this.state.placesVector.length} Results</Text> for {this.state.selectCategory}</Text>
                </View>
                
                <ScrollView>
                    <View style={{alignItems:'center', marginTop:20, flexDirection:'column'}}>
                        
                        {/*====== ITEMS ======*/}
                        {
                        this.state.placesVector.length != 0 && !this.state.loading?
                        this.state.placesVector.map((item) => {
                            // Opening Hour
                            var opening_hour = this.roundHour(new Date(item.opening_hours.open_hour).getUTCHours())
                            var ending_hour = this.roundHour(new Date(item.opening_hours.close_hour).getUTCHours())
                            var opening_minute = this.roundHour(new Date(item.opening_hours.open_hour).getUTCMinutes())
                            var ending_minute = this.roundHour(new Date(item.opening_hours.close_hour).getUTCMinutes())
                            
                            return (
                                <TouchableOpacity 
                                onPress={() => this.props.navigation.navigate('Place', {onSelect: this.onSelect, info: item, lat_user: LAT_USER, long_user: LONG_USER})}
                                style={{ 
                                  marginBottom: 30,
                                  width:width*0.9, 
                                  borderColor:'#C2C2C2', 
                                  borderWidth:1, 
                                  backgroundColor:'white',
                                  borderRadius:10,
                                  shadowColor: '#000000',
                                  shadowOpacity: 0.4,
                                  elevation: 1,
                                  shadowRadius: 5 ,
                                  shadowOffset : { width: 0, height: 7},}}
                                >  
                                <View style={{marginTop:28, position: 'absolute', width:5, height:113, backgroundColor:'#2177BB'}} /> 
                                <View style={{marginTop:28, right: -5, position: 'absolute', width:5, height:113, backgroundColor:'#2177BB'}} /> 
                                <View style={{paddingVertical:10}}>
                                  <View style={{paddingVertical:5, flexDirection:'row'}}>
                                    <View style={{left:10, justifyContent:'center', flex:0.15}}>
                                      <Image style={{width:48, height:45}} source={images.superMarkerListIcon} />
                                    </View>
                                    <View style={{flex:0.85, flexDirection:'column'}}>
                                      <View style={{marginHorizontal:10, flexDirection:'row', justifyContent:'space-between'}}>
                                        <View style={{width: width*0.4}}>
                                          <Text style={{marginTop:5, color:'#C2C2C2', fontSize:20}}>{item.name}</Text>
                                        </View>
                                        <View style={{borderRadius:5, paddingHorizontal:15, paddingVertical:5, backgroundColor:'#FEF2E5'}}>
                                          <Text style={{fontSize:18, color:'orange'}}>★ <Text style={{textDecorationLine: 'underline', fontWeight:'bold'}}>10mins</Text></Text>
                                        </View>
                                      </View>
                                      <View style={{marginHorizontal:10, flexDirection:'row', justifyContent:'space-between'}}>
                                        <Text style={{marginTop:10, color:'#C2C2C2', fontSize:18}}>{item.location.address} <Text style={{fontWeight:'bold'}}>• { item.location.units == "km" ?  (item.location.distance_to_origin).toFixed(2) : (item.location.distance_to_origin*1000).toFixed(2)}km</Text></Text>
                                      </View>
                                      <TouchableOpacity 
                                      onPress={()=>{this.props.navigation.navigate('Map', { info: item, lat_user: LAT_USER, long_user: LONG_USER})}}
                                      style={{marginTop:5, marginHorizontal:10, flexDirection:'row', justifyContent:'space-between'}}>
                                        <Text style={{fontSize:18, color:'#C2C2C2', fontWeight:'bold'}}>(Show on map)</Text>
                                      </TouchableOpacity>
                                      <View style={{marginTop:5, marginHorizontal:10, flexDirection:'row', justifyContent:'space-between'}}>
                                        <Text style={{fontSize:18}}>{opening_hour}:{opening_minute} - {ending_hour}:{ending_minute}</Text>
                                      </View>
                                      <View style={{marginTop:10, marginHorizontal:10, flexDirection:'row'}}>
                                        <Image style={{width:20, height:20, marginRight:5}} source={images.checkIcon} />
                                        <Text style={{bottom: -3}}>Check In</Text>
                                      </View>
                                    </View>
                                  </View>
                                </View>
                                </TouchableOpacity>
                            );
                        })
                        :
                        <ActivityIndicator style={{marginTop:20}} size="large" color="grey" />
                        }
                        
                    </View>
                </ScrollView>
          </View>
        </SideMenu>
      </SafeAreaView>
    );
  }
}


const styles = StyleSheet.create({
    button: {
      position: 'absolute',
      top: 20,
      padding: 10,
    },
    caption: {
      fontSize: 20,
      fontWeight: 'bold',
      alignItems: 'center',
    },
    container: {
      flex: 1,
      backgroundColor: 'white',
    },
    welcome: {
      fontSize: 20,
      textAlign: 'center',
      margin: 10,
    },
    instructions: {
      textAlign: 'center',
      color: '#333333',
      marginBottom: 5,
    },
    pickerStyle: {
        color:'red'
    }
  });