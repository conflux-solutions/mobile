import React from 'react';
import PropTypes from 'prop-types';
import {
  Dimensions,
  StyleSheet,
  ScrollView,
  View,
  Image,
  Text,
  TouchableOpacity,
} from 'react-native';
import images from '@assets/imageGeneral';

const window = Dimensions.get('window');
const uri = 'https://pickaface.net/gallery/avatar/Opi51c74d0125fd4.png';

const styles = StyleSheet.create({
  menu: {
    flex: 1,
    width: window.width,
    height: window.height,
    backgroundColor: '#4F5660'
  },
  avatarContainer: {
    marginBottom: 20,
    paddingBottom: 20,
    flexDirection:'row',
    paddingHorizontal:20,
    paddingVertical:30,
    backgroundColor:'#0A4A84',
  },
  avatar: {
    width: 48,
    height: 48,
    borderRadius: 24,
    flex: 1,
  },
  name: {
    marginLeft:10,
    color:'white',
    fontWeight:'bold',
    fontSize:18
  },
  item: {
    marginHorizontal:10,
    paddingVertical:20,
    fontSize: 14,
    fontWeight: '300',
    paddingTop: 5,
    //textTransform: 'uppercase',
    color:'#D2D2D2',
    fontWeight:'bold'
  },
});


export default function Menu({ onItemSelected }) {
  return (
    <ScrollView scrollsToTop={false} style={styles.menu}>
      <View style={styles.avatarContainer}>
        <View style={{alignItems:'center', marginTop:10, flexDirection:'row'}}> 
            <Image style={{width: 50, height: 50}} source={images.iconRound} />
            <View style={{flexDirection:'column'}}>
                <Text style={styles.name}>Menú </Text>
                <Text style={styles.name}>opciones</Text>
            </View>
        </View>
      </View>
      <TouchableOpacity 
      onPress={() => onItemSelected('SCAN_QR')}
      style={{marginTop:10, marginLeft:10, flexDirection:'row'}}>
        <Image style={{width: 27, height: 27}} source={images.qrWhiteIcon} />
        <Text style={styles.item}> Scan QR </Text>
      </TouchableOpacity>
      <TouchableOpacity 
      onPress={() => onItemSelected('MY_CHECKIN')}
      style={{marginTop:10, marginLeft:10, flexDirection:'row'}}>
        <Image style={{width: 27, height: 27}} source={images.libretaIcon} />
        <Text style={styles.item}>My Check IN</Text>
      </TouchableOpacity>
      <TouchableOpacity 
      onPress={() => onItemSelected('DETECT_LOCATION')}
      style={{marginTop:10, marginLeft:10, flexDirection:'row'}}>
        <Image style={{width: 19, height: 27, marginHorizontal:4}} source={images.gpsIconWhite} />
        <Text style={styles.item}> Detect my Location </Text>
      </TouchableOpacity>
      <TouchableOpacity 
      onPress={() => onItemSelected('LOG_OUT')}
      style={{marginTop:10, marginLeft:10, flexDirection:'row'}}>
        <Image style={{width: 20, height: 20, marginHorizontal:4}} source={images.logoutIcon} />
        <Text style={styles.item}> Log out </Text>
      </TouchableOpacity>
    </ScrollView>
  );
}

Menu.propTypes = {
  onItemSelected: PropTypes.func.isRequired,
};