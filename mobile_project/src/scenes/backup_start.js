import React from 'react';
import {
    View,
    Image,
    StyleSheet,
    TextInput,
    TouchableOpacity,
    Text,
    Dimensions,
    Button,
    KeyboardAvoidingView
  } from 'react-native';
import ConfirmGoogleCaptcha from 'react-native-google-recaptcha-v2';
const siteKey = '6LfnUegUAAAAAJY-EaFH6lQ3cVTXujk7138Z2So_';
const baseUrl = 'http://aftsolutions.co';

import AsyncStorage from '@react-native-community/async-storage';
import apiRoute from '@routes/apiRoute';

export default class App extends React.Component  {

    constructor(props) {
        super(props);
        this.state = {
            tryAgain: false
        };
      }
    
    storeData = async (key, value) => {
        try {
            await AsyncStorage.setItem(key, value)
        } catch (e) {
            // saving error
            console.warn("cath error", e)
        }
    }

    async componentDidMount() {
        const id_person = await AsyncStorage.getItem('id_person');
        console.warn("id_person", id_person)
        if(id_person == null) {
            this.captchaForm.show()
        } else {
            this.props.navigation.navigate('Geolocation')
        }
    }

    // async componentWillMount() {
    //     const id_person = await AsyncStorage.getItem('id_person');
    //     console.warn("id_person", id_person)
    //     if(id_person != null || id_person != "") {
    //         this.props.navigation.navigate('Geolocation')
    //     } else {
    //         this.captchaForm.show()
    //     }
    // }

    onMessage = event => {
         if (event && event.nativeEvent.data) {
            if (['cancel', 'error', 'expired'].includes(event.nativeEvent.data)) {
                this.captchaForm.hide();
                this.setState({tryAgain: true})
                return;
            } else {
                console.warn("SUCCESS")
                console.log('Verified code from Google', event.nativeEvent.data);
                this.storeData('captcha_confirm', "true")
                this.captchaConfirm()
                setTimeout(() => {
                    this.captchaForm.hide();
                    this.props.navigation.navigate('Geolocation')
                    // do what ever you want here
                }, 1500);
            }
        }
    };

    captchaConfirm(){
        var finalUrl = apiRoute.api + "users/new"
        try{
            fetch(finalUrl,{
                method: 'POST',
                headers:{
                Accept: 'application/json',
                'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                captcha_confirmation: "SUCCESS",
                }),
                }).then((response) => {
                console.log("responsex3", response)
                if(response.status == 200 || response.status == 201){
                    response.json().then((json) => {
                    console.log("LOGx2", json.user.id_person)
                    this.storeData('id_person', json.user.id_person)
                    })
                } else {
                  alert("Hubo un problema")
                }
                })
        }catch(error){
            console.warn(error)
        }
    } 

    
    render() {
        return (
            <View style={{alignItems:'center'}}>
                <ConfirmGoogleCaptcha
                    ref={_ref => this.captchaForm = _ref}
                    siteKey={siteKey}
                    baseUrl={baseUrl}
                    languageCode='en'
                    onMessage={this.onMessage}
                />
                <TouchableOpacity style={{marginTop:100}}  onPress={()=>{this.props.navigation.navigate('Geolocation')}}>
                <Text>start</Text>
            </TouchableOpacity>
            { this.state.tryAgain &&
            <TouchableOpacity onPress={()=>{this.captchaForm.show()}}>
                <Text>Try Again</Text>
            </TouchableOpacity>
            }
            </View>
            
        );
    }
}