import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Dimensions,
  StatusBar,
  ActivityIndicator,
  ScrollView, 
  ImageBackground,
  SafeAreaView,
} from 'react-native';
import SideMenu from 'react-native-side-menu';
import Menu from './menu';
import images from '@assets/imageGeneral';
import apiRoute from '@routes/apiRoute';
import AsyncStorage from '@react-native-community/async-storage';

var LAT_USER = 0
var LONG_USER = 0
var TOKEN_USER = ""

const { width, height } = Dimensions.get('window');


export default class myCheckIn extends Component {

  constructor(props) {
    super(props);

    this.state = {
      isOpen: false,
      shiftsVector: [],
      loading: true,
      today: true,
      previous: false,
      upcoming: false
    };
  }

componentDidMount() {
  console.warn("this.state.shiftsVector", this.state.shiftsVector.length)
  this.getStorageData()
}

toggle() {
  console.warn("1")
  this.setState({isOpen: !this.state.isOpen});
}

updateMenuState(isOpen) {
this.setState({ isOpen });
}

async getStorageData() {
  const tokenStorage = await AsyncStorage.getItem('token');
  console.warn("tokenStorage", tokenStorage)
  const user_latitued = await AsyncStorage.getItem('user_latitued');
  console.warn("user_latitued", user_latitued)
  const user_longitude = await AsyncStorage.getItem('user_longitude');
  console.warn("user_longitude", user_longitude)
  LAT_USER = parseInt(user_latitued)
  LONG_USER = parseInt(user_longitude)
  this.getMyShifts(tokenStorage)
}

  onMenuItemSelected = item => {
    console.warn("item", item)
    if(item=="DETECT_LOCATION") {
      this.props.navigation.navigate('Geolocation')
    } else if(item=="OWNER_REGISTER") {
      this.props.navigation.navigate('OwnerRegister')
    } else  if(item=="SCAN_QR") {
      this.props.navigation.navigate('ReadQRS')
    } else if (item=="OWNER_LOGIN") {
      this.props.navigation.navigate('OwnerLogin')
    }
    this.setState({
      isOpen: false,
      selectedItem: item,
    });
  }

  roundHour(value) {
    var response = ""
    response = value < 9 ? '0'+value : value
    return response
  }

  getMyShifts(tokenStorage) {
    console.warn("tokenStorage x1", tokenStorage)
    var finalUrl = apiRoute.api + "shifts/get-by-userid/"
      try{
          fetch(finalUrl,{
              method: 'GET',
              headers:{
              Accept: 'application/json',
              'Content-Type': 'application/json',
              'Authorization': tokenStorage
              },
              }).then((response) => {
              console.log("responsex3", response)
              
              if(response.status == 200 || response.status == 201){
                  response.json().then((json) => {
                    if(json.success) {
                      this.setState({loading:false})
                      let auxShiftsVector = []
                      json.shifts.forEach(element => {
                        auxShiftsVector.push(element)
                      });
                      this.setState({shiftsVector:auxShiftsVector})
                    } else {
                      alert("Something went wrong")
                    }
                  })
              } else {
                response.json().then((json) => {
                  console.log("JSON NO 200: ", json)
                  alert(json.message)
                  this.setState({loading:false})
                })
              }
              })
      }catch(error){
          console.warn(error)
      }
  }

  selectImage(ro) {
    switch (ro) {
      case "Pharmacy":
        return images.health;
      case "Bakery":
        return images.food;
      case "Food":
        return images.food;
      case "Transit":
        return images.bus;
      case "Supermarket":
        return images.market;
      case "Grocery":
        return images.market;
      case "Others":
        return images.more;
      case "Bank":
        return images.pig;
      default:
        return images.pig
    }
  }
  render() {

    const menu = <Menu onItemSelected={this.onMenuItemSelected} />;
    const placeholder = {
        label: 'Select a sport..',
        value: null,
        color: '#9EA0A4',
    };
    const placeholderDistance = {
        label: 'Select a distance..',
        value: null,
        color: '#9EA0A4',
    };

    return (
      <SafeAreaView>
          <StatusBar barStyle = "dark-content" hidden = {false} backgroundColor = "#00BCD4" translucent = {true}/>
          <Text style={{marginTop:height}}>CAM</Text>
        <SideMenu
          menu={menu}
          isOpen={this.state.isOpen}
          onChange={isOpen => this.updateMenuState(isOpen)}
        >
          <View style={styles.container}>
                <ImageBackground style={{width:width, height:120}} source={images.gradientHeader}>
                  <View style={{marginTop: 30, width: width, height: 70, }}>
                      <View>
                          <View style={{flexDirection:'row'}}>
                              <TouchableOpacity onPress={() => (this.props.navigation.navigate('Categories'))}>
                                <View style={{marginLeft:15, width:width*0.1,  marginTop:15}}>
                                  <Image style={{width:45, height:55}} source={images.logoIcon} />
                                </View>
                              </TouchableOpacity>
                              <View style={{marginLeft:10, width:width*0.68,  marginTop:20}}>
                                
                              </View>
                              <View style={{marginHorizontal:10, width:width*0.1, marginTop:22}}>
                                  <TouchableOpacity
                                  onPress={()=>{this.updateMenuState(!this.state.isOpen)}}
                                  >
                                      <Image style={{width:42, height:40}} source={images.menuIconWhite} />
                                  </TouchableOpacity>
                              </View>
                          </View>
                      </View>
                  </View>
                </ImageBackground>
                
                <View style={{paddingHorizontal:60, marginBottom:10, marginTop: 10,alignItems:'center', flexDirection:'row', justifyContent:'space-between'}}>
                  <TouchableOpacity style={{alignItems:'center'}}
                  onPress={()=>{this.setState({today: false, previous: true, upcoming: false})}}>
                    <Text style={{color: this.state.previous?'black':'grey'}}>Previous</Text>
                    <View style={{width:80, height: this.state.previous?1:0, backgroundColor:'black'}} />
                  </TouchableOpacity>
                  <TouchableOpacity 
                  onPress={()=>{this.setState({today: true, previous: false, upcoming: false})}}
                  style={{alignItems:'center'}}>
                    <Text style={{color: this.state.today?'black':'grey'}}>Today</Text>
                    <View style={{width:60, height: this.state.today?1:0, backgroundColor:'black'}} />
                  </TouchableOpacity>
                  <TouchableOpacity style={{alignItems:'center'}}
                  onPress={()=>{this.setState({today: false, previous: false, upcoming: true})}}
                  >
                    <Text style={{color: this.state.upcoming?'black':'grey'}}>Upcoming</Text>
                    <View style={{width:80, height: this.state.upcoming?1:0, backgroundColor:'black'}} />
                  </TouchableOpacity>
                </View>
                
                {this.state.today ?

                <ScrollView>
                    <View style={{alignItems:'center', marginTop:20, flexDirection:'column'}}>
                        
                        {/*====== ITEMS ======*/}
                        {
                        this.state.shiftsVector.length != 0 && !this.state.loading?
                        this.state.shiftsVector.map((item) => {
                         
                            // GET DATE TODAY FORMAT
                            var dateToday = new Date()
                            let dayShift = dateToday.getDate()
                            let montShift = dateToday.getUTCMonth()+1
                            let yearShift = dateToday.getFullYear()
                            dateToday = montShift+"/"+dayShift+"/"+yearShift

                            // GET DATE FROM SHIFT
                            var dateShift = item.shift_date_string.split(',')
                            dateShift = dateShift[0].replaceAll(' ','')
                            console.warn("shift_date_string", dateShift)

                            // ENTRY HOUR
                            var entry = item.shift_date_string
                            entry = new Date(entry)
                            // 
                            var entryHour = entry.setMinutes(entry.getMinutes())
                            let entryMinutes =  entry.getMinutes() < 10 ? "0"+entry.getMinutes() : entry.getMinutes()
                            entryHour = entry.getHours() + ":" + entryMinutes

                            // EXIT HOUR
                            let slot = item.establishment.slot_size
                            var finalEntry = entry.setMinutes(entry.getMinutes()+slot)
                            let minutes =  entry.getMinutes() < 10 ? "0"+entry.getMinutes() : entry.getMinutes()
                            finalEntry = entry.getHours() + ":" + minutes
                            
                            if(dateShift==dateToday) {
                            return (
                                <TouchableOpacity 
                                style={{ 
                                  marginBottom: 30,
                                  width:width*0.9, 
                                  borderColor:'#C2C2C2', 
                                  borderWidth:1, 
                                  backgroundColor:'white',
                                  borderRadius:10,
                                  shadowColor: '#000000',
                                  shadowOpacity: 0.4,
                                  elevation: 1,
                                  shadowRadius: 5 ,
                                  shadowOffset : { width: 0, height: 7},}}
                                >  
                                <View style={{paddingVertical:10}}>
                                  <View style={{paddingVertical:5, flexDirection:'row'}}>
                                    <View style={{left:10, justifyContent:'center', flex:0.15}}>
                                      <Image style={{width:48, height:45}} source={images.superMarkerListIcon} />
                                    </View>
                                    <View style={{flex:0.85, flexDirection:'column'}}>
                                      <View style={{marginHorizontal:15, flexDirection:'row', justifyContent:'space-between'}}>
                                        <View style={{width: width*0.7}}>
                                          <Text style={{marginTop:5, color:'#C2C2C2', fontSize:20}}>{item.establishment.name}</Text>
                                        </View>
                                      </View>
                                      <View style={{marginTop:10, marginHorizontal:15, flexDirection:'row', justifyContent:'space-between'}}>
                                        <Text style={{color:'#C2C2C2', fontSize:18}}>ENTRY</Text>
                                        <Text style={{color:'#C2C2C2', fontSize:18}}>{entryHour}</Text>
                                      </View>
                                      <View style={{marginTop:10, marginHorizontal:15, flexDirection:'row', justifyContent:'space-between'}}>
                                        <Text style={{color:'#C2C2C2', fontSize:18}}>EXIT</Text>
                                        <Text style={{color:'#C2C2C2', fontSize:18}}>{finalEntry}</Text>
                                      </View>
                                      <View style={{marginTop:10, marginHorizontal:15, flexDirection:'row', justifyContent:'space-between'}}>
                                        <Text style={{color:'#C2C2C2', fontSize:18}}>STATUS</Text>
                                        {item.shift_checked ?
                                        <Text style={{color:'#00D343', fontSize:18}}>Checked In</Text>
                                        :
                                        <Text style={{color:'#C2C2C2', fontSize:18}}>Not Checked</Text>}
                                      </View>
                                      <TouchableOpacity 
                                        onPress={()=>{this.props.navigation.navigate('Map', { info: item.establishment, lat_user: LAT_USER, long_user: LONG_USER})}}
                                        style={{marginTop:5, marginHorizontal:10, flexDirection:'row', justifyContent:'space-between'}}>
                                        <Text style={{fontSize:18, color:'#C2C2C2', fontWeight:'bold'}}>(Show on map)</Text>
                                      </TouchableOpacity>
                                    </View>
                                  </View>
                                </View>
                                </TouchableOpacity>
                            );
                          }
                        })
                        :
                        <ActivityIndicator style={{marginTop:20}} size="large" color="grey" />
                        }
                        
                    </View>
                </ScrollView>
                :
                null
                }

                {this.state.previous ?

                <ScrollView>
                    <View style={{alignItems:'center', marginTop:20, flexDirection:'column'}}>
                        
                        {/*====== ITEMS ======*/}
                        {
                        this.state.shiftsVector.length != 0 && !this.state.loading?
                        this.state.shiftsVector.map((item) => {
                        
                            // GET DATE TODAY FORMAT
                            var dateToday = new Date()
                            let dayShift = dateToday.getDate()
                            let montShift = dateToday.getUTCMonth()+1
                            let yearShift = dateToday.getFullYear()
                            dateToday = montShift+"/"+dayShift+"/"+yearShift
                            dateToday = Date.parse(new Date(dateToday))

                            // GET DATE FROM SHIFT
                            var dateShift = item.shift_date_string.split(',')
                            dateShift = dateShift[0].replaceAll(' ','')
                            console.warn("shift_date_string", dateShift)
                            dateShift = Date.parse(new Date(dateShift))
                        
                            // ENTRY HOUR
                            var entry = item.shift_date_string
                            entry = new Date(entry)
                            // 
                            var entryHour = entry.setMinutes(entry.getMinutes())
                            let entryMinutes =  entry.getMinutes() < 10 ? "0"+entry.getMinutes() : entry.getMinutes()
                            entryHour = entry.getHours() + ":" + entryMinutes

                            // EXIT HOUR
                            let slot = item.establishment.slot_size
                            var finalEntry = entry.setMinutes(entry.getMinutes()+slot)
                            let minutes =  entry.getMinutes() < 10 ? "0"+entry.getMinutes() : entry.getMinutes()
                            finalEntry = entry.getHours() + ":" + minutes
                            
                            if(dateShift<dateToday) {
                            return (
                                <TouchableOpacity 
                                style={{ 
                                  marginBottom: 30,
                                  width:width*0.9, 
                                  borderColor:'#C2C2C2', 
                                  borderWidth:1, 
                                  backgroundColor:'white',
                                  borderRadius:10,
                                  shadowColor: '#000000',
                                  shadowOpacity: 0.4,
                                  elevation: 1,
                                  shadowRadius: 5 ,
                                  shadowOffset : { width: 0, height: 7},}}
                                >  
                                <View style={{paddingVertical:10}}>
                                  <View style={{paddingVertical:5, flexDirection:'row'}}>
                                    <View style={{left:10, justifyContent:'center', flex:0.15}}>
                                      <Image style={{width:48, height:45}} source={images.superMarkerListIcon} />
                                    </View>
                                    <View style={{flex:0.85, flexDirection:'column'}}>
                                      <View style={{marginHorizontal:15, flexDirection:'row', justifyContent:'space-between'}}>
                                        <View style={{width: width*0.7}}>
                                          <Text style={{marginTop:5, color:'#C2C2C2', fontSize:20}}>{item.establishment.name}</Text>
                                        </View>
                                      </View>
                                      <View style={{marginTop:10, marginHorizontal:15, flexDirection:'row', justifyContent:'space-between'}}>
                                        <Text style={{color:'#C2C2C2', fontSize:18}}>ENTRY</Text>
                                        <Text style={{color:'#C2C2C2', fontSize:18}}>{entryHour}</Text>
                                      </View>
                                      <View style={{marginTop:10, marginHorizontal:15, flexDirection:'row', justifyContent:'space-between'}}>
                                        <Text style={{color:'#C2C2C2', fontSize:18}}>EXIT</Text>
                                        <Text style={{color:'#C2C2C2', fontSize:18}}>{finalEntry}</Text>
                                      </View>
                                      <View style={{marginTop:10, marginHorizontal:15, flexDirection:'row', justifyContent:'space-between'}}>
                                        <Text style={{color:'#C2C2C2', fontSize:18}}>STATUS</Text>
                                        {item.shift_checked ?
                                        <Text style={{color:'#00D343', fontSize:18}}>Checked In</Text>
                                        :
                                        <Text style={{color:'#C2C2C2', fontSize:18}}>Not Checked</Text>}
                                      </View>
                                      <TouchableOpacity 
                                        onPress={()=>{this.props.navigation.navigate('Map', { info: item.establishment, lat_user: LAT_USER, long_user: LONG_USER})}}
                                        style={{marginTop:5, marginHorizontal:10, flexDirection:'row', justifyContent:'space-between'}}>
                                        <Text style={{fontSize:18, color:'#C2C2C2', fontWeight:'bold'}}>(Show on map)</Text>
                                      </TouchableOpacity>
                                    </View>
                                  </View>
                                </View>
                                </TouchableOpacity>
                            );
                          }
                        })
                        :
                        <ActivityIndicator style={{marginTop:20}} size="large" color="grey" />
                        }
                        
                    </View>
                </ScrollView>
                :
                null
                }

                {this.state.upcoming ?

                <ScrollView>
                    <View style={{alignItems:'center', marginTop:20, flexDirection:'column'}}>
                        
                        {/*====== ITEMS ======*/}
                        {
                        this.state.shiftsVector.length != 0 && !this.state.loading?
                        this.state.shiftsVector.map((item) => {
                        
                            // GET DATE TODAY FORMAT
                            var dateToday = new Date()
                            let dayShift = dateToday.getDate()
                            let montShift = dateToday.getUTCMonth()+1
                            let yearShift = dateToday.getFullYear()
                            dateToday = montShift+"/"+dayShift+"/"+yearShift
                            dateToday = Date.parse(new Date(dateToday))

                            // GET DATE FROM SHIFT
                            var dateShift = item.shift_date_string.split(',')
                            dateShift = dateShift[0].replaceAll(' ','')
                            console.warn("shift_date_string", dateShift)
                            dateShift = Date.parse(new Date(dateShift))
                        
                            // ENTRY HOUR
                            var entry = item.shift_date_string
                            entry = new Date(entry)
                            // 
                            var entryHour = entry.setMinutes(entry.getMinutes())
                            let entryMinutes =  entry.getMinutes() < 10 ? "0"+entry.getMinutes() : entry.getMinutes()
                            entryHour = entry.getHours() + ":" + entryMinutes

                            // EXIT HOUR
                            let slot = item.establishment.slot_size
                            var finalEntry = entry.setMinutes(entry.getMinutes()+slot)
                            let minutes =  entry.getMinutes() < 10 ? "0"+entry.getMinutes() : entry.getMinutes()
                            finalEntry = entry.getHours() + ":" + minutes
                            
                            if(dateShift>dateToday) {
                            return (
                                <TouchableOpacity 
                                style={{ 
                                  marginBottom: 30,
                                  width:width*0.9, 
                                  borderColor:'#C2C2C2', 
                                  borderWidth:1, 
                                  backgroundColor:'white',
                                  borderRadius:10,
                                  shadowColor: '#000000',
                                  shadowOpacity: 0.4,
                                  elevation: 1,
                                  shadowRadius: 5 ,
                                  shadowOffset : { width: 0, height: 7},}}
                                >  
                                <View style={{paddingVertical:10}}>
                                  <View style={{paddingVertical:5, flexDirection:'row'}}>
                                    <View style={{left:10, justifyContent:'center', flex:0.15}}>
                                      <Image style={{width:48, height:45}} source={images.superMarkerListIcon} />
                                    </View>
                                    <View style={{flex:0.85, flexDirection:'column'}}>
                                      <View style={{marginHorizontal:15, flexDirection:'row', justifyContent:'space-between'}}>
                                        <View style={{width: width*0.7}}>
                                          <Text style={{marginTop:5, color:'#C2C2C2', fontSize:20}}>{item.establishment.name}</Text>
                                        </View>
                                      </View>
                                      <View style={{marginTop:10, marginHorizontal:15, flexDirection:'row', justifyContent:'space-between'}}>
                                        <Text style={{color:'#C2C2C2', fontSize:18}}>ENTRY</Text>
                                        <Text style={{color:'#C2C2C2', fontSize:18}}>{entryHour}</Text>
                                      </View>
                                      <View style={{marginTop:10, marginHorizontal:15, flexDirection:'row', justifyContent:'space-between'}}>
                                        <Text style={{color:'#C2C2C2', fontSize:18}}>EXIT</Text>
                                        <Text style={{color:'#C2C2C2', fontSize:18}}>{finalEntry}</Text>
                                      </View>
                                      <View style={{marginTop:10, marginHorizontal:15, flexDirection:'row', justifyContent:'space-between'}}>
                                        <Text style={{color:'#C2C2C2', fontSize:18}}>STATUS</Text>
                                        {item.shift_checked ?
                                        <Text style={{color:'#00D343', fontSize:18}}>Checked In</Text>
                                        :
                                        <Text style={{color:'#C2C2C2', fontSize:18}}>Not Checked</Text>}
                                      </View>
                                      <TouchableOpacity 
                                        onPress={()=>{this.props.navigation.navigate('Map', { info: item.establishment, lat_user: LAT_USER, long_user: LONG_USER})}}
                                        style={{marginTop:5, marginHorizontal:10, flexDirection:'row', justifyContent:'space-between'}}>
                                        <Text style={{fontSize:18, color:'#C2C2C2', fontWeight:'bold'}}>(Show on map)</Text>
                                      </TouchableOpacity>
                                    </View>
                                  </View>
                                </View>
                                </TouchableOpacity>
                            );
                          }
                        })
                        :
                        <ActivityIndicator style={{marginTop:20}} size="large" color="grey" />
                        }
                        
                    </View>
                </ScrollView>
                :
                null
                }
          </View>
        </SideMenu>
      </SafeAreaView>
    );
  }
}


const styles = StyleSheet.create({
    button: {
      position: 'absolute',
      top: 20,
      padding: 10,
    },
    caption: {
      fontSize: 20,
      fontWeight: 'bold',
      alignItems: 'center',
    },
    container: {
      flex: 1,
      backgroundColor: 'white',
    },
    welcome: {
      fontSize: 20,
      textAlign: 'center',
      margin: 10,
    },
    instructions: {
      textAlign: 'center',
      color: '#333333',
      marginBottom: 5,
    },
    pickerStyle: {
        color:'red'
    }
  });