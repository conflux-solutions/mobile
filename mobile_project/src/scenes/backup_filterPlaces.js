import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Dimensions,
  StatusBar,
  Picker,
  ActivityIndicator,
  ScrollView,
  Button,
  SafeAreaView,
} from 'react-native';
import SideMenu from 'react-native-side-menu';
import Menu from './menu';
import RNPickerSelect from 'react-native-picker-select';
import images from '@assets/imageGeneral';
import apiRoute from '@routes/apiRoute';

var LAT_USER = 0
var LONG_USER = 0

var NAME_CAT = ""
const { width, height } = Dimensions.get('window');

const colors = {
    red:'#DF6E6E',
    green:'#b7eb8f',
    yellow:'#DF6E6E',
    grey:'#DF6E6E'
}
const distance = [
    {
        label: '500m',
        value: '500m',
    },
    {
      label: '1000m',
      value: '1 Km',
    },
    {
      label: '2000m',
      value: '2 Kms',
    },
    {
      label: '4000m',
      value: '4 Kms',
    },
    {
      label: '8000m',
      value: '8 Kms',
    },
  ];

const places = [
    {
      label: 'Farmacia',
      value: 'Pharmacie',
    },
    {
      label: 'Hospital',
      value: 'Hospital',
    },
    {
      label: 'Tienda/Viveres',
      value: 'Grocery',
    },
    {
      label: 'Panadería',
      value: 'Bakery',
    },
    {
      label: 'Super Mercado',
      value: 'Supermarket',
    },
]

export default class FilterPlaces extends Component {

  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);

    this.state = {
      isOpen: false,
      selectedItem: 'About',
      pruebaItem: 'Pharmacie',
      placesVector: [],
      loading: true,
      radius: "500m",
      firstTime: true,
      loading: true
    };
  }

componentDidMount() {
    this.getSentData()
    console.warn("this.state.placesVector", this.state.placesVector.length)
}

/// Function to get sent data from categories screen
///
/// Data sent lat_user, long_user, id_category, name_category
///
getSentData() {
    const lat_user =  this.props.navigation.getParam('lat_user', 'nothing sent')
    const long_user =  this.props.navigation.getParam('long_user', 'nothing sent')
    
    const name_category =  this.props.navigation.getParam('name_category', 'nothing sent')
    
    console.warn("LAT_USER " + lat_user);
    console.warn("LONG_USER " + long_user);
    
    console.warn("NAME_CAT " + name_category);
    LAT_USER = lat_user
    LONG_USER = long_user
    NAME_CAT = name_category
    
    this.setState({selectCategory: NAME_CAT, loading: false})
    this.getPlaces(NAME_CAT, LAT_USER, LONG_USER, this.state.radius)
}

/// Method to get establishment by category and location
///
/// GET
///
/// Params: category, latitude, longitude, radius
///
/// Additional Information: 
/* Radius:
 -  500m = 500m
 -  1km = 1%20km
 -  2km = 2%20km
*/
///    
getPlaces(category, lat, long, radius) {
    console.warn("Data", category, lat, long, radius)
    this.setState({loading: true})
    var finalUrl = apiRoute.api + "establishment?lat="+lat+"&lng="+long+"&radius="+radius+"&category="+category+"&offset=0&limit=10"
    console.warn("finalUrl", finalUrl)
    console.log("finalUrl", finalUrl)

    fetch(finalUrl, {
        method: 'GET'
        //Request Type 
    })
    .then((response) => response.json())
    //If response is in json then in success
    .then((responseJson) => {
        //Success 
        //console.warn("responseJson", responseJson.values)
        
        responseJson.values.forEach(element => {
            //console.warn("element", element)
            console.log("responseJsonx2", element.location)
        });
        this.setState({placesVector: responseJson.values, loading: false})
        console.warn("placesVector", this.state.placesVector)
    })
    //If response is not in json then in error
    .catch((error) => {
        //Error 
        //alert(JSON.stringify(error));
        console.error(error);
    });
  }

  onSelect = data => {
    
    // if(data.selected) {
    //   this.setState({
    //     showFinalPlace: true
    //   })
    // }
    this.setState(data);
    this.getPlaces("Pharmacie", LAT_USER, LONG_USER, "500m")
  };

  toggle() {
      console.warn("1")
    this.setState({
      isOpen: !this.state.isOpen,
    });
  }

  updateMenuState(isOpen) {
    this.setState({ isOpen });
  }

  goBack() {
    const { navigation } = this.props;
    navigation.goBack();
}

  onMenuItemSelected = item => {
    console.warn("item", item)
    if(item=="DETECT_LOCATION") {
      this.props.navigation.navigate('Geolocation')
    } else if(item=="OWNER_REGISTER") {
      this.props.navigation.navigate('OwnerRegister')
    } else  if(item=="SCAN_QR") {
      this.props.navigation.navigate('ReadQRS')
    } else if (item=="OWNER_LOGIN") {
      this.props.navigation.navigate('OwnerLogin')
    }
    this.setState({
      isOpen: false,
      selectedItem: item,
    });
  }


  render() {

    const menu = <Menu onItemSelected={this.onMenuItemSelected} />;
    const placeholder = {
        label: 'Select a sport..',
        value: null,
        color: '#9EA0A4',
    };
    const placeholderDistance = {
        label: 'Select a distance..',
        value: null,
        color: '#9EA0A4',
    };

    return (
      <SafeAreaView>
          <StatusBar barStyle = "dark-content" hidden = {false} backgroundColor = "#00BCD4" translucent = {true}/>
          <Text style={{marginTop:height}}>CAM</Text>
        <SideMenu
          menu={menu}
          isOpen={this.state.isOpen}
          onChange={isOpen => this.updateMenuState(isOpen)}
        >
          <View style={styles.container}>
                <View style={{marginTop:10, width: width, height: 70, backgroundColor:'#FAFAFA', borderBottomWidth:0.25, borderBottomColor:'grey'}}>
                    <View>
                        <View style={{flexDirection:'row'}}>
                            <View style={{width:width*0.1,  marginTop:40}}>
                                <TouchableOpacity 
                                onPress={() => this.goBack()}
                                style={{marginLeft:5, marginTop:-5}}>
                                    <Image style={{width:25, height:25}} source={images.backIcon} />
                                </TouchableOpacity>
                            </View>
                            <View style={{alignItems:'center', borderRadius:5, justifyContent:'center', marginTop:38, width: width*0.8}}>
                            <Text style={{fontWeight:'bold', fontSize:18}}>Establecimientos: {NAME_CAT}</Text>
                            </View>
                            <View style={{width:width*0.1, marginTop:35}}>
                                <TouchableOpacity
                                onPress={()=>{this.props.navigation.navigate('ReadQRS')}}
                                >
                                    <Image style={{width:30, height:30}} source={images.qrIconB} />
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </View>
                {
                  !this.state.loading?

                <View style={{flexDirection:'row', marginTop:20, justifyContent:'space-between'}}>
                    <View style={{marginLeft:10}}>
                        <View style={{ justifyContent: 'center',  
                                        borderWidth:0.25, 
                                        borderColor: 'grey',
                                        width:180, height:30}}>
                            
                                <View style={{marginLeft:10}}>
                                  <RNPickerSelect
                                      placeholder={{
                                        label: `${this.state.selectCategory}`,
                                        value: null,
                                    }}
                                    style={styles.pickerStyle}
                                    onValueChange={(value) => {this.setState({selectCategory: value}), this.getPlaces(value, LAT_USER, LONG_USER, this.state.radius)}}
                                    items={places}
                                    Icon={() => {
                                        return <Image style={{marginRight:5, width:25, height:20}} source={images.downIcon} /> ;
                                      }}
                                />
                                </View>
                            </View> 
                        
                    </View> 
                    <View style={{marginRight:10}}>
                        <View style={{ justifyContent: 'center',  
                                        borderWidth:0.25, 
                                        borderColor: 'grey',
                                        width:120, height:30}}>
                            <View style={{flexDirection:'row'}}>
                                <View style={{marginLeft:10, flex:0.99}}>
                                    <RNPickerSelect
                                        placeholder={{
                                          label: `${this.state.radius}`,
                                          value: null
                                        }}
                                        onValueChange={(value) => {this.setState({radius:value}), this.getPlaces(this.state.selectCategory, LAT_USER, LONG_USER, value)}}
                                        items={distance}
                                        Icon={() => {
                                            return <Image style={{marginRight:5, width:25, height:20}} source={images.downIcon} /> ;
                                          }}
                                    />
                                </View>
                            </View>
                        </View>
                    </View>
                </View>
                : null }
                <ScrollView>
                    <View style={{marginLeft:10, marginTop:30, flexDirection:'column'}}>
                        {/*====== ITEMS ======*/}
                        {
                        this.state.placesVector.length != 0 && !this.state.loading?
                        this.state.placesVector.map((item) => {
                            return (
                                <TouchableOpacity 
                                onPress={() => this.props.navigation.navigate('Place', {onSelect: this.onSelect, info: item, lat_user: LAT_USER, long_user: LONG_USER})}
                                style={{borderTopLeftRadius: this.state.placesVector.indexOf(item)==0?10:0, 
                                                            borderTopRightRadius: this.state.placesVector.indexOf(item)==0?10:0, 
                                                            backgroundColor:'#FAFAFA',  
                                                            borderBottomLeftRadius: this.state.placesVector.length-1 == this.state.placesVector.indexOf(item) ? 10:0,  
                                                            borderBottomRightRadius: this.state.placesVector.length-1 == this.state.placesVector.indexOf(item) ? 10:0,  
                                                            borderBottomWidth: this.state.placesVector.length-1 == this.state.placesVector.indexOf(item) ? 0.5:0,  
                                                            alignItems:'center', justifyContent: 'space-between', 
                                                            flexDirection:'row', borderWidth:0.5, borderColor:'grey', 
                                                            width:width*0.95, height:50}}>
                                    <View style={{flex:0.9, marginLeft:10}}>
                                        <Text>{item.name}</Text>
                                    </View>      
                                    <View style={{alignItems:'center', flexDirection:'row', marginRight:20}}>
                                        <Text>{(item.location.distance_to_origin*1000).toFixed(2)} m</Text>
                                        {/*<View style={{marginLeft:10, width:25, height:25, borderRadius:50, backgroundColor: item.status=="empty"? colors.green:colors.red}}/>*/}
                                    </View>       
                                </TouchableOpacity>
                            );
                        })
                        :
                        <ActivityIndicator style={{marginTop:20}} size="large" color="grey" />
                        }
                        
                    </View>
                </ScrollView>
          </View>
        </SideMenu>
      </SafeAreaView>
    );
  }
}


const styles = StyleSheet.create({
    button: {
      position: 'absolute',
      top: 20,
      padding: 10,
    },
    caption: {
      fontSize: 20,
      fontWeight: 'bold',
      alignItems: 'center',
    },
    container: {
      flex: 1,
      backgroundColor: 'white',
    },
    welcome: {
      fontSize: 20,
      textAlign: 'center',
      margin: 10,
    },
    instructions: {
      textAlign: 'center',
      color: '#333333',
      marginBottom: 5,
    },
    pickerStyle: {
        color:'red'
    }
  });