import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Dimensions,
  StatusBar,
  Picker,
  ActivityIndicator,
  ScrollView,
  Button,
  ImageBackground,
  SafeAreaView,
  TextInput
} from 'react-native';
import SideMenu from 'react-native-side-menu';
import Menu from './menu';
import RNPickerSelect from 'react-native-picker-select';
import images from '@assets/imageGeneral';
import apiRoute from '@routes/apiRoute';
import {Calendar} from 'react-native-calendars';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import AsyncStorage from '@react-native-community/async-storage';

// LocaleConfig.locales['en'] = {
//     monthNames: ['Janvier','Février','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','Décembre'],
//     monthNamesShort: ['Janv.','Févr.','Mars','Avril','Mai','Juin','Juil.','Août','Sept.','Oct.','Nov.','Déc.'],
//     dayNames: ['Dimanche','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi'],
//     dayNamesShort: ['Dim.','Lun.','Mar.','Mer.','Jeu.','Ven.','Sam.'],
//     today: 'Aujourd\'hui'
//   };
// LocaleConfig.defaultLocale = 'en';

var LAT_USER = 0
var LONG_USER = 0
var fecha = '2020-05-20'

var LATITUDE = 0;
var LONGITUDE = 0;
var CATEGORY = "";
var INFO

var NAME_CAT = ""
const { width, height } = Dimensions.get('window');
var TOKEN_USER = ""
var ID_ESTABLISHMENT = ""


export default class Book extends Component {

  constructor(props) {
    super(props);
    //*
    //*
    /// PLACE LOCATION
    const info =  this.props.navigation.getParam('info', 'nothing sent')
    console.warn("LONG " + info.location.longitude);
    console.warn("LAT " + info.location.latitude);
    //*
    //*
    /// SET DATA PLACE
    INFO = info
    console.warn("INFO", INFO)
    CATEGORY = info.category
    LATITUDE = info.location.latitude
    LONGITUDE = info.location.longitude

    this.toggle = this.toggle.bind(this);

    this.state = {
      isOpen: false,
      selectedItem: 'About',
      pruebaItem: 'Pharmacie',
      loading: true,
      radius: "500m",
      firstTime: true,
      selectedTime: '',
      slot_hours: []
    };
  }

componentDidMount() {
    this.getSentData()
    this.getCurrentDate()
    const id_establishment =  this.props.navigation.getParam('id_establishment', 'no id_establishment sent')
    console.warn("id_establishment " + id_establishment);
    ID_ESTABLISHMENT = id_establishment
}

async componentWillMount() {
  const tokenStorage = await AsyncStorage.getItem('token');
  TOKEN_USER = tokenStorage
  console.warn("tokenStorage", tokenStorage)
  const user_latitude = await AsyncStorage.getItem('user_latitude')
  LAT_USER = user_latitude
  console.log("entra", LAT_USER)
  const user_longitud = await AsyncStorage.getItem('user_longitude')
  LONG_USER = user_longitud
}

getCurrentDate() {
    this.setState({loading: true})

    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();

    today = yyyy + '-' + mm + '-' + dd;
    this.setState({selected: today, todayDate: today})
    // GET CURRENT DATE IN EPOCH FORMAT
    setTimeout(() => {
      // write your functions    
      this.convertToEpochFormat(true)
   }, 2000);

    
}

/// Function to get sent data from categories screen
///
/// Data sent lat_user, long_user, id_category, name_category
///
getSentData() {
    const name_category =  this.props.navigation.getParam('name_category', 'nothing sent')
    NAME_CAT = name_category
    this.setState({selectCategory: NAME_CAT, loading: false})
}

/// Method to get establishment by category and location
///
/// GET
///
/// Params: category, latitude, longitude, radius
///
/// Additional Information: 
/* Radius:
 -  500m = 500m
 -  1km = 1%20km
 -  2km = 2%20km
*/
///    

  onSelect = data => {
    
    // if(data.selected) {
    //   this.setState({
    //     showFinalPlace: true
    //   })
    // }
    this.setState(data);
    
  };

  toggle() {
      console.warn("1")
    this.setState({
      isOpen: !this.state.isOpen,
    });
  }

  updateMenuState(isOpen) {
    this.setState({ isOpen });
  }

  goBack() {
    const { navigation } = this.props;
    navigation.goBack();
}

  onMenuItemSelected = item => {
    console.warn("item", item)
    if(item=="DETECT_LOCATION") {
      this.props.navigation.navigate('Geolocation')
    } else if(item=="OWNER_REGISTER") {
      this.props.navigation.navigate('OwnerRegister')
    } else  if(item=="SCAN_QR") {
      this.props.navigation.navigate('ReadQRS')
    } else if (item=="OWNER_LOGIN") {
      this.props.navigation.navigate('OwnerLogin')
    }
    this.setState({
      isOpen: false,
      selectedItem: item,
    });
  }

  searchFunction = value => {
    console.warn("value", value)
    this.setState({valueSearch: value})
  }

  onDayPress = (day) => {
    this.setState({selected: day.dateString});
    this.convertToEpochFormat(false)
  }

  convertToEpochFormat(today) {
    this.setState({loading: false})
    console.warn("this.state.todayDate", this.state.todayDate)
    console.warn("today", today)
    console.warn("TOKEN", TOKEN_USER)
    console.warn("id_Esta", ID_ESTABLISHMENT)
    let date = today ? this.state.todayDate : this.state.selected
    let timeToConvert = date+"T00:00:00.000000"
    var unix_seconds = ((new Date(timeToConvert)).getTime()) /1000;
    unix_seconds = unix_seconds*1000
    this.searchTimeSlots(unix_seconds)
  }

  dateToEpochFormat() {
    console.warn("TOKEN_USER", TOKEN_USER)
    if(this.state.selectedTime != "") {
      var dateSelected = this.state.selected
      let time = this.state.selectedTime
      dateSelected = dateSelected.replaceAll('-','/')
      let timeNDate = dateSelected+" "+time
      let timeToSent = Date.parse(new Date(timeNDate))
      console.warn("timeToSent", timeToSent)
      if(TOKEN_USER != "") {
        this.sendShift(timeNDate, timeToSent)
      } else {
        console.warn("Token Error || Session expired")
      }
    } else {
      alert("No time selected")
    }
  }

  searchTimeSlots(time) {
    var finalUrl = apiRoute.api + "shifts/get-by-date/?id_establishment="+ID_ESTABLISHMENT+"&date="+time
      try{
          fetch(finalUrl,{
              method: 'GET',
              headers:{
              'Authorization': TOKEN_USER
              },
              }).then((response) => {
              console.log("searchTimeSlot Response: ", response)
              if(response.status == 200 || response.status == 201){
                  response.json().then((json) => {
                  var slot_hours_array = []
                  if(json.success) {
                    console.log("ResponseJSON: ", json)
                    json.slots.forEach(element => {
                      if(element.available)
                       slot_hours_array.push({'value':element.slot_hour_start, 'label':element.slot_hours})
                    }); 
                  }
                  this.setState({slot_hours: slot_hours_array})
                  })
              } else {
                alert("Hubo un problema 1")
              }
              })
      }catch(error){
          console.warn(error)
      }
  }
  sendShift(timeNDate, time) {
      console.warn("timeNDate", timeNDate)
      console.warn("time", time)
      console.warn("ID_ESTABLISHMENT", ID_ESTABLISHMENT)
      console.log("lat_user", LAT_USER)
      console.log("long_user", LONG_USER)
      this.props.navigation.navigate('ConfirmShift', {time: time, timeNDate: timeNDate, info: INFO, lat_user: LAT_USER, long_user: LONG_USER})

  }

  render() {

    const menu = <Menu onItemSelected={this.onMenuItemSelected} />;
    const placeholder = {
        label: 'Select a sport..',
        value: null,
        color: '#9EA0A4',
    };
    const placeholderDistance = {
        label: 'Select a distance..',
        value: null,
        color: '#9EA0A4',
    };


    return (
      <SafeAreaView>
          <StatusBar barStyle = "dark-content" hidden = {false} backgroundColor = "#00BCD4" translucent = {true}/>
          <Text style={{marginTop:height}}>CAM</Text>
        <SideMenu
          menu={menu}
          isOpen={this.state.isOpen}
          onChange={isOpen => this.updateMenuState(isOpen)}
        >
          <View style={styles.container}>
                <ImageBackground style={{width:width, height:120}} source={images.gradientHeader}>
                  <View style={{marginTop: 30, width: width, height: 70, }}>
                      <View>
                          <View style={{flexDirection:'row'}}>
                              <View style={{marginLeft:15, width:width*0.1,  marginTop:15}}>
                                <Image style={{width:45, height:55}} source={images.logoIcon} />
                              </View>
                              <View style={{marginLeft:10, width:width*0.68,  marginTop:20}}>
                                <View style={{height:45, marginHorizontal:10, borderRadius:7, alignItems:'center', backgroundColor:'#0E4877', flexDirection:'row'}}>
                                
                                  <TextInput 
                                    style={{color:'white', marginTop:4, width:width*0.7, paddingHorizontal:10, height:30}} 
                                    maxLength={20}
                                    onChangeText={text => this.searchFunction(text)}
                                    >
                                  </TextInput>
                                  <Image style={{position:'absolute', right:12, width:25, height:25}} source={images.searchIcon} />

                                </View>
                              </View>
                              <View style={{marginHorizontal:10, width:width*0.1, marginTop:22}}>
                                  <TouchableOpacity
                                  onPress={()=>{this.updateMenuState(!this.state.isOpen)}}
                                  >
                                      <Image style={{width:42, height:40}} source={images.menuIconWhite} />
                                  </TouchableOpacity>
                              </View>
                          </View>
                      </View>
                  </View>
                </ImageBackground>
                
                {/*=== Float QR Button START =====*/}
                {
                  height < 700 ? null : 
                  <TouchableOpacity 
                  onPress={()=>{this.props.navigation.navigate('ReadQRS')}}
                  style={{
                          position:'absolute', 
                          zIndex: 3, //ioS
                          elevate: 3,  //Android
                          right:20, bottom:100,
                          borderRadius:50, alignItems:'center', 
                          justifyContent: 'center', 
                          width:55, height:55, 
                          backgroundColor:'#2177BB'}}>
                  <Image style={{width:35, height:35}} source={images.qrBKBlue} />
                </TouchableOpacity>
                }
                
                {/*=== Float QR Button END =====*/}
                <View style={{alignItems:'center', marginLeft:10, marginTop:10}}>
                    <Text>RESERVE YOUR SHIFT</Text>
                    {/*======== Calendar START ========*/}
                    <Calendar
                        style={{width:width*0.98}}
                        onDayPress={this.onDayPress}
                        onDayLongPress={(day) => {console.log('selected day', day)}}
                        onMonthChange={(month) => {console.log('month changed', month)}}
                        firstDay={1}
                        minDate={this.state.todayDate}
                        hideDayNames={false}
                        
                        showWeekNumbers={false}
                        onPressArrowLeft={substractMonth => substractMonth()}
                        onPressArrowRight={addMonth => addMonth()}
                        disableArrowLeft={false}
                        disableArrowRight={false}
                        markedDates={{
                            [this.state.selected]: {
                            selected: true, 
                            disableTouchEvent: true, 
                            selectedDotColor: 'orange'
                            }
                        }}     
                    />
                    {/*======= Calendar END ===========*/}
                </View>
                <View style={{marginVertical:20, alignSelf:'center', width: width*0.9, borderWidth:0.5, borderColor:'grey' }} />
                <View style={{flexDirection:'row', alignContent:'center', marginLeft:20, marginTop:20, width:150}}>
                  <Text style={{marginRight: 10}}>Select an Hour Range</Text>
                  <Text style={{marginRight: 10}}>-</Text>
                  <RNPickerSelect
                      placeholder={{
                        label: `Touch to select`,
                        value: null
                      }}
                      onValueChange={(value) => {console.log(value), this.setState({selectedTime:value})}}
                      items={this.state.slot_hours}
                  />
                </View>
                {
                this.state.loading ?
                <ActivityIndicator style={{marginTop:20}} size="large" color="grey" />
                :
                <View style={{width:width*0.5, marginTop:40, justifyContent:'space-between', alignSelf:'center', flexDirection:'row'}}>
                    <TouchableOpacity style={{paddingVertical:10, paddingHorizontal:20}}>
                      <Text style={{fontSize:20}}>Cancel</Text>
                    </TouchableOpacity>
                    <TouchableOpacity 
                    onPress={()=>{this.dateToEpochFormat()}}
                    style={{borderRadius:25, paddingVertical:10, paddingHorizontal:20, borderWidth:2, borderColor:'#2177BB'}}>
                      <Text style={{fontSize:20, fontWeight:'bold', color:'#2177BB'}}>Confirm</Text>
                    </TouchableOpacity>
                </View>
                }
          </View>
        </SideMenu>
      </SafeAreaView>
    );
  }
}


const styles = StyleSheet.create({
    button: {
      position: 'absolute',
      top: 20,
      padding: 10,
    },
    caption: {
      fontSize: 20,
      fontWeight: 'bold',
      alignItems: 'center',
    },
    container: {
      flex: 1,
      backgroundColor: 'white',
    },
    welcome: {
      fontSize: 20,
      textAlign: 'center',
      margin: 10,
    },
    instructions: {
      textAlign: 'center',
      color: '#333333',
      marginBottom: 5,
    },
    pickerStyle: {
        color:'red'
    }
  });