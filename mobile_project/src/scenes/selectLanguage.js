import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Dimensions,
  StatusBar,
  ActivityIndicator
} from 'react-native';
import images from '@assets/imageGeneral';
import language from '@languages/languagesGeneral'

import AsyncStorage from '@react-native-community/async-storage';

var HASNOTCH = false
var LAT_USER = 0
var LONG_USER = 0
var EXISTS_TOKEN = ""
const { width, height } = Dimensions.get('window');

export default class SelectLanguage extends Component {

  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      category: [],
      selectedLanguageVar: language.en,
      selectedLanguage: 'en',
      languages: [ 
        { 
            "name":'English',
            "code": 'en',
            "selected": language.en
        },
        { 
            "name":'Español',
            "code": 'es',
            "selected": language.es
        },
        { 
            "name":'Français',
            "code": 'fr',
            "selected": language.fr
        },
    ]

    };
  }

componentDidMount() {
}

storeData = async (key, value) => {
  try {
    await AsyncStorage.setItem(key, value)
  } catch (e) {
    // saving error
    console.warn("cath error", e)
  }
}


processData() {
    console.warn("this.state.selectedLanguage", this.state.selectedLanguage)
    this.storeData('selected_language', this.state.selectedLanguage)
    this.setState({loading:true})
    setTimeout(() => { 
        this.setState({loading:false})
        this.props.navigation.navigate('Categories')
    },2500);

}

  render() {

    return (
        <View style={styles.container}>
            <StatusBar barStyle = "dark-content" hidden = {false} backgroundColor = "#00BCD4" translucent = {true}/>
                <View style={{marginTop:40, alignItems:'center', width: width, height: 120}}>
                    <Image style={{width:58, height:85}} source={images.logoIconText} />
                  </View>
                <View style={{flex: 1}}>
                    <View style={{alignItems:'center'}}>
                        <Text style={{fontSize:20}}>{this.state.selectedLanguageVar.selectLanguage}</Text>
                    </View>
                    <View style={{marginTop:40, alignItems:'center', flexDirection:'column'}}>
                        {
                            this.state.languages.map((item) => {
                                return (
                                    <TouchableOpacity
                                    onPress={()=>{this.setState({selectedLanguage: item.code, selectedLanguageVar: item.selected})}}>
                                    <View style={{marginTop:20, width:width*0.5, flexDirection:'row', alignItems:'center', justifyContent:'space-between'}}>
                                        <View>
                                            <Text style={{fontWeight:this.state.selectedLanguage==item.code?'bold':''}}>{item.name}</Text>
                                        </View>
                                            <View style={{alignItems:'center', justifyContent:'center', width:25, height:25, borderWidth:this.state.selectedLanguage==item.code?2:1, borderColor: this.state.selectedLanguage==item.code?'green':'grey'}}>
                                                <Text style={{color:'green'}}>
                                                {this.state.selectedLanguage==item.code?"✔":""}
                                                </Text>
                                            </View>
                                    </View>
                                </TouchableOpacity>
                                )})
                        } 
                    </View>
                    <TouchableOpacity
                    onPress={()=>{this.processData()}} 
                    style={{alignItems:'center', justifyContent:'center', backgroundColor:'#2177BB', width:width*0.7, height:48, alignSelf:'center', position:'absolute', bottom:50}}>
                    {
                        this.state.loading ?
                        <ActivityIndicator size="small" color="white" />
                        :
                        <Text style={{color:'white', fontWeight:'bold'}}>{this.state.selectedLanguageVar.continue}</Text>
                    }
                    </TouchableOpacity>
                </View>
        </View>
    );
  }
}


const styles = StyleSheet.create({
    button: {
      position: 'absolute',
      top: 20,
      padding: 10,
    },
    caption: {
      fontSize: 20,
      fontWeight: 'bold',
      alignItems: 'center',
    },
    container: {
      flex: 1,
      backgroundColor: 'white',
    },
    welcome: {
      fontSize: 20,
      textAlign: 'center',
      margin: 10,
    },
    instructions: {
      textAlign: 'center',
      color: '#333333',
      marginBottom: 5,
    },
    pickerStyle: {
        color:'red'
    }
  });