import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Dimensions,
  StatusBar,
  Picker,
  ImageBackground,
  ActivityIndicator,
  ScrollView,
  Button,
  TextInput,
  SafeAreaView,
} from 'react-native';
import SideMenu from 'react-native-side-menu';
import Menu from './menu';
import RNPickerSelect from 'react-native-picker-select';
import images from '@assets/imageGeneral';
import apiRoute from '@routes/apiRoute';
import language from '@languages/languagesGeneral'

var LAT_USER = 0
var LONG_USER = 0

const { width, height } = Dimensions.get('window');

const colors = {
    red:'#DF6E6E',
    green:'#b7eb8f',
    yellow:'#DF6E6E',
    grey:'#DF6E6E'
}
export default class Place extends Component {

  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);

    this.state = {
      isOpen: false,
      selectedItem: 'About',
      pruebaItem: 'Pharmacie',
      placesVector: [],
      loading: true,
      radius: "500m"
    };
  }

componentDidMount() {
    this.shuffleArray()
}

goBack() {
    const { navigation } = this.props;
    navigation.goBack();
    navigation.state.params.onSelect({ selected: true });
}

shuffleArray() {
    var ro = Math.floor(Math.random() * (3));
    this.setState({randomN:ro})
    console.warn("random", ro)
}

selectBK(ro){
    switch(ro){
      case 0:
        return images.pharmacie1
        break;
      case 1:
        return images.pharmacie2
        break;
      case 2:
        return images.pharmacie3
        break;
  }
}


  toggle() {
      console.warn("1")
    this.setState({
      isOpen: !this.state.isOpen,
    });
  }

  updateMenuState(isOpen) {
    console.warn("2")
    this.setState({ isOpen });
  }

  onMenuItemSelected = item => {
    console.warn("item", item)
    if(item=="DETECT_LOCATION") {
      this.props.navigation.navigate('Geolocation')
    } else if(item=="OWNER_REGISTER") {
      this.props.navigation.navigate('OwnerRegister')
    } else  if(item=="SCAN_QR") {
      this.props.navigation.navigate('ReadQRS')
    } else if (item=="OWNER_LOGIN") {
      this.props.navigation.navigate('OwnerLogin')
    } else if (item=="LOG_OUT"){
      console.warn("LOG_OUT")
      this.clearItem()
    } else if (item=="MY_CHECKIN") {
      this.props.navigation.navigate('MyCheckIn')
    }
    this.setState({
      isOpen: false,
      selectedItem: item,
    });
  }

    roundHour(value) {
      var response = ""
      response = value < 9 ? '0'+value : value
      return response
    }

    setDay(day) {
      console.warn("day", day)
        switch (day) {
          case 1:
            return "Monday"
          case 2:
            return "Tuesday"
          case 3: 
            return "Wednesday"
          case 4:
            return "Thursday"
          case 5:
            return "Friday"
          case 6:
            return "Saturday"
          case 0:
            return "Sunday"
         }
    }

  render() {

    const { state, goBack } = this.props.navigation;
    ///Data sent
    //*
    //*
    /// USER LOCATION
    //*
    const lat_user =  this.props.navigation.getParam('lat_user', 'nothing sent')
    const long_user =  this.props.navigation.getParam('long_user', 'nothing sent')
    console.warn("LAT_USER PLACE" + lat_user);
    console.warn("LONG_USER PLACE" + long_user);
    LAT_USER = lat_user
    LONG_USER = long_user
    //*
    /// INFO PLACE
    //*
    const info =  this.props.navigation.getParam('info', 'nothing sent')
    console.warn("PROPS " + info.name);

    const menu = <Menu onItemSelected={this.onMenuItemSelected} /> 
    const placeholder = {
        label: 'Select a sport..',
        value: null,
        color: '#9EA0A4',
    };
    const placeholderDistance = {
        label: 'Select a distance..',
        value: null,
        color: '#9EA0A4',
    };

    // Opening Hour
    var opening_hour = this.roundHour(new Date(info.opening_hours.open_hour).getUTCHours())
    var ending_hour = this.roundHour(new Date(info.opening_hours.close_hour).getUTCHours())
    var opening_minute = this.roundHour(new Date(info.opening_hours.open_hour).getUTCMinutes())
    var ending_minute = this.roundHour(new Date(info.opening_hours.close_hour).getUTCMinutes())
    // CHECK CURRENT DAT
    var opening_day = ""
    var currentWeekDay = new Date().getDay()
    console.warn("currentWeekDay", currentWeekDay)
    
    let minDay = Math.min(...info.opening_hours.day.map(item => item))
    console.warn("minDay", minDay)
    
    info.opening_hours.day.forEach(element => {
      opening_day = element==currentWeekDay ? "Today" : this.setDay(minDay)
      
    });
    

    return (


      <SafeAreaView>
          <StatusBar barStyle = "dark-content" hidden = {false} backgroundColor = "#00BCD4" translucent = {true}/>
          <Text style={{marginTop:height}}></Text>
        <SideMenu
          menu={menu}
          isOpen={this.state.isOpen}
          onChange={isOpen => this.updateMenuState(isOpen)}
        >
          <View style={styles.container}>
                <ImageBackground style={{width:width, height:120}} source={images.gradientHeader}>
                  <View style={{marginTop: 30, width: width, height: 70, }}>
                      <View>
                          <View style={{flexDirection:'row'}}>
                              <View style={{marginLeft:15, width:width*0.1,  marginTop:15}}>
                                <Image style={{width:45, height:55}} source={images.logoIcon} />
                              </View>
                              <View style={{marginLeft:10, width:width*0.68,  marginTop:20}}>
                                <View style={{height:45, marginHorizontal:10, borderRadius:7, alignItems:'center', backgroundColor:'#0E4877', flexDirection:'row'}}>
                                
                                  <TextInput 
                                    style={{color:'white', marginTop:4, width:width*0.7, paddingHorizontal:10, height:30}} 
                                    maxLength={20}
                                    onChangeText={text => console.log("search", text)}
                                    >
                                  </TextInput>
                                  <Image style={{position:'absolute', right:12, width:25, height:25}} source={images.searchIcon} />

                                </View>
                              </View>
                              <View style={{marginHorizontal:10, width:width*0.1, marginTop:22}}>
                                  <TouchableOpacity
                                  onPress={()=>{this.updateMenuState(!this.state.isOpen)}}
                                  >
                                      <Image style={{width:42, height:40}} source={images.menuIconWhite} />
                                  </TouchableOpacity>
                              </View>
                          </View>
                      </View>
                  </View>
                </ImageBackground>

                <StatusBar barStyle = "dark-content" hidden = {false} backgroundColor = "#00BCD4" translucent = {true}/>

                <View style={styles.container}>
               
                <TouchableOpacity 
                      onPress={()=>{this.props.navigation.navigate('ReadQRS')}}
                      style={{
                              position:'absolute', 
                              zIndex: 3, //ioS
                              elevate: 3,  //Android
                              right:20, bottom:30,
                              borderRadius:50, alignItems:'center', 
                              justifyContent: 'center', 
                              width:55, height: 55, 
                              backgroundColor:'#2177BB'}}>
                        <Image style={{width:35, height:35}} source={images.qrBKBlue} />
                </TouchableOpacity>

                <TouchableOpacity 
                  style={{position:'absolute', 
                  zIndex: 3, //ioS
                  elevate: 3,  //Android
                  bottom: 30, left:10, }}
                  onPress={()=>{this.props.navigation.navigate('Book', {id_establishment: info._id, info: info, lat_user: LAT_USER, long_user: LONG_USER})}}>
                  <Image style={{
                  borderRadius:5, height:40, width:170}} source={images.bookNowIcon} />
                </TouchableOpacity>

                <ScrollView scrollEnabled={height>600?false:true}>
                  
                    <View style={{marginHorizontal:10, marginVertical:10}}>
                        <Image style={{width:width*0.95, height:250}} source={this.selectBK(this.state.randomN)} />
                        <TouchableOpacity
                        style={{shadowOpacity: 1.2, shadowRadius: 2,
                          elevation: 1, shadowColor: 'gray', shadowOffset: { width: 2, height: 6 }, width:50, height:50, alignItems:'center', justifyContent: 'center', borderRadius:50, marginBottom:10, flexDirection:'row', backgroundColor:'white', paddingVertical:10,  position:'absolute', bottom:0, right:10}}
                        onPress={()=>{console.warn("TOUCH"), this.props.navigation.navigate('Map', { info: info, lat_user: LAT_USER, long_user: LONG_USER})}}
                        >
                            <Image style={{width:35, height:35}} source={images.iconShowMap} />
                        </TouchableOpacity>
                    </View>
                    
                    
                    
                    <View style={{alignSelf:'center', marginVertical:10, width:40, height:1, backgroundColor:'grey'}} />
                      <View style={{marginHorizontal:20, flexDirection:'row', justifyContent:'space-between'}}>
                        <View style={{flexDirection:'column'}}>
                          <Text style={{fontSize:18}} >{info.name}</Text>
                          <Text style={{fontWeight:'bold', color:'grey'}} >{info.category}</Text>
                          <View style={{marginTop:20, flexDirection:'row'}}>
                            <Image style={{width:25, height:25}} source={images.detailsLocationIcon} />
                            <Text style={{marginLeft:10, fontWeight:'bold',  marginTop:2, color:'grey'}} >{info.location.address}</Text>
                          </View>
                          <View style={{marginLeft:25, flexDirection:'row' }}>
                            <Text style={{marginLeft:10, fontWeight:'bold', color:'grey'}} >{info.location.city}, </Text>
                            <Text style={{fontWeight:'bold', color:'grey'}} >{info.location.country}</Text>
                          </View>
                          <View style={{marginTop:10, flexDirection:'row'}}>
                            <Image style={{width:25, height:25}} source={images.detailsPhoneIcon} />
                            <Text style={{marginLeft:10, fontWeight:'bold',  marginTop:2, color:'grey'}} >{info.phone}</Text>
                          </View>
                          <View style={{marginTop:10, flexDirection:'row'}}>
                            <Image style={{width:25, height:25}} source={images.detailsTimeIcon} />
                            <Text style={{marginLeft:10, fontWeight:'bold',  marginTop:2, color:'grey'}}>{opening_day} - {opening_hour}:{opening_minute}-{ending_hour}:{ending_minute}</Text>
                          </View>
                        </View>
                        <View style={{borderWidth:2, borderRadius:10, height:30, borderColor:'#E9FFE5', backgroundColor:'#ACFB91', paddingVertical:5, paddingHorizontal:15, flexDirection:'column'}}>
                          <Text style={{color:'grey', fontWeight:'bold'}}>★  <Text style={{textDecorationLine:'underline'}}>{info.shift_attention_mins} mins</Text></Text>
                        </View>
                    </View>
                </ScrollView>
                </View>


          </View>
        </SideMenu>
      </SafeAreaView>
      
      
    );
  }
}


const styles = StyleSheet.create({
    button: {
      position: 'absolute',
      top: 20,
      padding: 10,
    },
    caption: {
      fontSize: 20,
      fontWeight: 'bold',
      alignItems: 'center',
    },
    container: {
      flex: 1,
      backgroundColor: 'white',
    },
    welcome: {
      fontSize: 20,
      textAlign: 'center',
      margin: 10,
    },
    instructions: {
      textAlign: 'center',
      color: '#333333',
      marginBottom: 5,
    },
    pickerStyle: {
        color:'red'
    }
  });