/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Button,
  PermissionsAndroid,
  Platform,
  StyleSheet,
  Image,
  Text,
  ToastAndroid,
  ActivityIndicator,
  Dimensions,
  View,
  TouchableOpacity
} from 'react-native';
import Geolocation from 'react-native-geolocation-service';
import images from '@assets/imageGeneral';
import AsyncStorage from '@react-native-community/async-storage';

var LAT_USER = 0
var LONG_USER = 0
var ALREADY_LANG_SELECT = ""
const { width, height } = Dimensions.get('window');

export default class App extends Component<{}> {
  watchId = null;

  state = {
    loading: false,
    updatesEnabled: false,
    location: {},
    address: ''
  };

  componentDidMount = async () =>  {
    this.getLocation()
  }

  async componentWillMount() {
    const id_person = await AsyncStorage.getItem('id_person');
    console.warn("id_person", id_person)
    const already_lang_selected = await AsyncStorage.getItem('selected_language');
    ALREADY_LANG_SELECT = already_lang_selected
    console.log("ALREADY_LANG_SELECT", ALREADY_LANG_SELECT)
  }

  storeData = async (key, value) => {
    try {
      await AsyncStorage.setItem(key, value)
    } catch (e) {
      // saving error
      console.warn("cath error", e)
    }
  }

  getLocation = async () => {
    const hasLocationPermission = await this.hasLocationPermission();
    if(hasLocationPermission){
        Geolocation.getCurrentPosition(
            (position) => {
                console.warn("position", position);
                this.getAddressLocation(position.coords.latitude, position.coords.longitude)
            },
            (error) => {
                // See error code charts below.
                console.log(error.code, error.message);
            },
            { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
        );
    }
  }
  getAddressLocation(newLat, newLong){
    //GET request 
    fetch('http://www.mapquestapi.com/geocoding/v1/reverse?key=ZpqIwWpZHnJq6wwJL6hdPz5n4wVwXaZg&location='+newLat+','+newLong+'&includeRoadMetadata=true&includeNearestIntersection=true', {
        method: 'GET'
        //Request Type 
    })
    .then((response) => response.json())
    //If response is in json then in success
    .then((responseJson) => {
        //Success 
        ADDRESS = responseJson.results[0].locations[0].street + ", " + responseJson.results[0].locations[0].adminArea1 + ", " + responseJson.results[0].locations[0].adminArea3
        
        console.warn("ADDRESS",ADDRESS)
        this.storeData('user_latitude', newLat.toString())
        this.storeData('user_longitude', newLong.toString())
        

        LAT_USER = newLat
        LONG_USER = newLong 
        
        this.setState({
            address: ADDRESS
        })
    })
    //If response is not in json then in error
    .catch((error) => {
        //Error 
        //alert(JSON.stringify(error));
        console.error(error);
    });
  }

  hasLocationPermission = async () => {
    if (Platform.OS === 'ios' ||
        (Platform.OS === 'android' && Platform.Version < 23)) {
      return true;
    }

    const hasPermission = await PermissionsAndroid.check(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
    );

    if (hasPermission) return true;

    const status = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
    );

    if (status === PermissionsAndroid.RESULTS.GRANTED) return true;

    if (status === PermissionsAndroid.RESULTS.DENIED) {
      ToastAndroid.show('Location permission denied by user.', ToastAndroid.LONG);
    } else if (status === PermissionsAndroid.RESULTS.NEVER_ASK_AGAIN) {
      ToastAndroid.show('Location permission revoked by user.', ToastAndroid.LONG);
    }

    return false;
  }

  render() {
    const { loading, location, updatesEnabled } = this.state;
    return (
      <View style={styles.container}>
        <Button title="Obtenir l'emplacement" onPress={this.getLocation} disabled={loading || updatesEnabled} />
            <Image
                style={{marginTop:10, width: 50, height:50}}
                source={images.pinGps}    
            />
            {this.state.address == '' ?
            <ActivityIndicator style={{marginTop:20}} size="large" color="grey" />
            :
            <View style={{flexDirection:'row', width:width*0.7}}>
                <Text style={{textAlign:'center', justifyContent: 'center', alignItems:'center'}}>{this.state.address}</Text>
            </View>
            }
            <TouchableOpacity 
                onPress={() => ALREADY_LANG_SELECT ==null ? this.props.navigation.navigate('SelectLanguage', {lat_user: LAT_USER, long_user: LONG_USER}) : this.props.navigation.navigate('Categories')}
                style={{ marginTop:15, flexDirection: 'row',
                    backgroundColor: '#001529', borderRadius: 20,
                    height:50, width: width*0.7,
                    alignItems: 'center', justifyContent: 'center',
                    marginBottom:10, borderBottomWidth: 0,
                    shadowColor: 'gray', shadowOffset: { width: 2, height: 6 },
                    shadowOpacity: 1.2, shadowRadius: 2,
                    elevation: 1}}>
                <Text style={{color:'white'}}>Confirmer l'emplacement</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={()=>{this.getLocation()}} style={{position:'absolute', bottom:20}}>
                <Text> Not location?, Retry</Text>
            </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
    paddingHorizontal: 12
  },
  result: {
      borderWidth: 1,
      borderColor: '#666',
      width: '100%',
      paddingHorizontal: 16
  },
  buttons: {
      flexDirection: 'row',
      justifyContent: 'space-around',
      alignItems: 'center',
      marginVertical: 12,
      width: '100%'
  }
});