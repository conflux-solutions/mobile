import React from 'react';
import {
  Text,
  StyleSheet,
  TouchableOpacity,
  View,
  Image,
  Dimensions
} from 'react-native';
import images from '@assets/imageGeneral';

var {height, width} = Dimensions.get('window');

function selectImage(ro) {
  switch (ro) {
    case "Pharmacy":
      return images.health;
    case "Bakery":
      return images.food;
    case "Coffee":
      return images.food;
    case "Transport":
      return images.bus;
    case "Supermarket":
      return images.market;
    case "Grocery":
      return images.market;
    case "More":
      return images.more;
    case "Bank":
      return images.pig;
    default:
      return images.pig
  }
}
function SFilter(props) {
  
  return (
        
        <View style={{marginHorizontal: 20, 
                      marginVertical: 20,
                      justifyContent:'center', 
                      alignItems:'center', 
                      height:80, 
                      width:80,
                      backgroundColor:'#2077BB', 
                      borderColor:'#E8EDF2', borderWidth:1}}>
                      <Image style={{width:40, height:40}} source={selectImage(props.title.en)} />
                      <Text style={{color:'white', top:3}}>{props.title[props.lan]}</Text>
        </View>
    
  )
}

const styles = StyleSheet.create({
  
})

export default SFilter
